﻿using RPG.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Stats
{
    // DataBase for getting a base stat for certain level (the formulas can be different for each character)
    [CreateAssetMenu(fileName = "Progression", menuName = "Stats/Progression", order = 0)]
    public class Progression : ScriptableObject
    {

        //list of each class
        [SerializeField] ProgressionStat[] progressionStats = null;

        Dictionary<Stat, ProgressionStatFormula> lookupTable = null;

        public float GetStat(Stat statToGet, int level)
        {
            BuildLookup();
            if (lookupTable.ContainsKey(statToGet))
            {
                return lookupTable[statToGet].Calculate(level);
            }
            return 0;
        }

        // build the table when first accessed
        private void BuildLookup()
        {
            if (lookupTable != null) return;
            lookupTable = new Dictionary<Stat, ProgressionStatFormula>();

            foreach (var progressionStat in progressionStats)
            {

                lookupTable[progressionStat.stat] = progressionStat.statFormula;
            }
            
        }



        //class for single stat
        [System.Serializable]
        class ProgressionStat
        {
            public Stat stat = Stat.Experience;
            public ProgressionStatFormula statFormula;
        }

    }
}


