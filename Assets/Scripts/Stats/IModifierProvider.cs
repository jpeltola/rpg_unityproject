﻿using System.Collections.Generic;

namespace RPG.Stats
{
    // Interface that BaseStats uses when getting a stat (such as damage). BaseStats goes through all IModifierProvider components and gets the additive and multiplicative modifiers.
    // Some IModifierProviders are: StatsEquipment, Effect
    public interface IModifierProvider
    {
        IEnumerable<float> GetAdditiveModifiers(Stat stat);
        IEnumerable<float> GetPercentageModifiers(Stat stat);
    }
}