﻿
namespace RPG.Stats
{
    // Different stat types
    public enum Stat
    {
        Experience,
        ExperienceToLevelUp,
        Health,
        Damage,
        Strength,
        Intelligence,
        Agility,
        Range,
        AttackSpeed,
        MovementSpeed,
        HealthRegeneration
    }
}