﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Stats
{
    // UI that displays current xp and next level xp
    public class ExperienceDisplay : MonoBehaviour
    {

        Experience experience;
        BaseStats baseStats;

        private void Awake()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
            baseStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
        }

        private void Update()
        {
            GetComponent<Text>().text = String.Format("{0:0}/{1:0}", Mathf.Floor(experience.GetPoints()),Mathf.Ceil(baseStats.GetStat(Stat.ExperienceToLevelUp,1)));
        }
    }
}
