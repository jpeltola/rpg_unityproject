﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{
    // Component that holds a data for a quest. When put on a npc, that npc can give the quest, when certain event is triggered (for example, dialogue)
    public class QuestGiver : MonoBehaviour
    {
        [SerializeField] Quest quest;

        public void GiveGuest()
        {
            QuestList questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
            if (questList.HasQuest(quest)) return;
            questList.AddQuest(quest);
        }
    }
}
