﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{
    // Holds data for the status of a certain quest (which objectives are completed etc.)

    [System.Serializable]
    public class QuestStatus
    {
        [SerializeField] Quest quest;
        [SerializeField] List<string> completedObjectives = new List<string>();

        [System.Serializable]
        class QuestStatusRecord
        {
            public string questName;
            public List<string> completedObjectives;
        }

        public QuestStatus(Quest quest)
        {
            this.quest = quest;
        }

        public QuestStatus(object questRecord)
        {
            QuestStatusRecord record = questRecord as QuestStatusRecord;

            quest = Quest.GetByName(record.questName);
            completedObjectives = record.completedObjectives;
        }

        public bool IsComplete()
        {
            return quest.GetObjectiveCount() == completedObjectives.Count;
        }

        public Quest GetQuest()
        {
            return quest;
        }
        public int GetCompletedObjectiveCount()
        {
            return completedObjectives.Count;
        }

        public IEnumerable<string> GetCompletedObjectives()
        {
            return completedObjectives;
        }

        public void CompleteObjective(string objective)
        {
            if (quest.HasObjective(objective) && !completedObjectives.Contains(objective))
            {
                completedObjectives.Add(objective);
            }
            
        }

        public bool IsCompleted(string objective)
        {
            return completedObjectives.Contains(objective);
        }

        public object CaptureState()
        {
            QuestStatusRecord record = new QuestStatusRecord();
            record.questName = quest.GetTitle();
            record.completedObjectives = completedObjectives;
            return record;
        }
    }

}