﻿using RPG.Core;
using RPG.Inventories;
using RPG.Saving;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{
    // List of quests that the player have. Also responsibe for evaluating the predicates: HasQuest and QuestCompleted etc..
    public class QuestList : MonoBehaviour, ISaveable, IPredicateEvaluator
    {
        List<QuestStatus> statuses = new List<QuestStatus>();

        public event Action onUpdate;

        public void AddQuest(Quest quest)
        {
            QuestStatus newStatus = new QuestStatus(quest);
            statuses.Add(newStatus);

            if(onUpdate != null)
            {
                onUpdate();
            } 
        }

        public void CompleteObjective(Quest quest, string objective)
        {
            QuestStatus status = GetQuestStatus(quest);
            if (status == null) return;
            status.CompleteObjective(objective);
            GiveObjeciveRewards(quest, objective);
            RemoveRequiredItems(quest, objective);
            if(status.IsComplete())
            {
                GiveRewards(quest);
            }
            if(onUpdate != null)
            {
                onUpdate();
            }
            
        }

        private void RemoveRequiredItems(Quest quest, string objective)
        {
            List<InventoryItem> items = quest.GetObjectiveRequiredItems(objective) as List<InventoryItem>;
            if (items == null) return;
            Inventory inventory = Inventory.GetPlayerInventory();
            foreach(var item in items)
            {
                if(inventory.HasItem(item,out int slot))
                {
                    inventory.RemoveFromSlot(slot, 1);
                }
            }
   
        }

        private void GiveObjeciveRewards(Quest quest, string objective)
        {
            foreach(var reward in quest.GetObjectiveRewards(objective))
            {
                bool success = GetComponent<Inventory>().AddToFirstEmptySlot(reward.item, reward.number);
                if (!success)
                {
                    GetComponent<ItemDropper>().DropItem(reward.item, reward.number);
                }
            }
        }

        public bool HasQuest(Quest quest)
        {
            return GetQuestStatus(quest) != null;
        }

        private QuestStatus GetQuestStatus(Quest quest)
        {
            foreach (QuestStatus status in statuses)
            {
                if (status.GetQuest() == quest)
                {
                    return status;
                }
            }
            return null;
        }

        private void GiveRewards(Quest quest)
        {
            foreach(var reward in quest.GetRewards())
            {
                bool success = GetComponent<Inventory>().AddToFirstEmptySlot(reward.item, reward.number);
                if(!success)
                {
                    GetComponent<ItemDropper>().DropItem(reward.item,reward.number);
                }
            }
        }

        public IEnumerable<QuestStatus> GetStatuses()
        {
            return statuses;
        }

        public object CaptureState()
        {
            List<object> state = new List<object>();
            foreach(QuestStatus status in statuses)
            {
                state.Add(status.CaptureState());
            }
            return state;
        }


        public void RestoreState(object state)
        {
            List<object> stateList = state as List<object>;
            if (state == null) return;

            statuses.Clear();

            foreach(object objectState in stateList)
            {
                statuses.Add(new QuestStatus(objectState));
            }
        }

        public bool? Evaluate(EPredicate predicate, string[] parameters)
        {
            if (parameters.Length == 0) return null;
            switch(predicate)
            {
                case EPredicate.HasQuest:
                    return HasQuest(Quest.GetByName(parameters[0]));
                case EPredicate.QuestCompleted:
                    if (GetQuestStatus(Quest.GetByName(parameters[0])) == null) return false;
                    return GetQuestStatus(Quest.GetByName(parameters[0])).IsComplete();
            }
            return null;

        }
    }
}
