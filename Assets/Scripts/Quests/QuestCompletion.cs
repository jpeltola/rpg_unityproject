﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{
    // Component that is responsible for completing a quest
    public class QuestCompletion : MonoBehaviour
    {
        [SerializeField] Quest quest;
        [SerializeField] string objectiveRef;

        // If some event is triggered, this can be called to complete the objective in a quest ( for example ending a dialogue)
        public void CompleteObjective()
        {
            QuestList questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
            questList.CompleteObjective(quest, objectiveRef);
        }
    }
}