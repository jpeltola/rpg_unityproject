﻿using RPG.Inventories;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.Quests
{
    // Holds the data for single quest (objectives and rewards) 
    [CreateAssetMenu(fileName = "New Quest", menuName = "Quest", order = 0)]
    public class Quest : ScriptableObject
    {
        [SerializeField] List<Objective> objectives = new List<Objective>();
        [SerializeField] List<Reward> rewards = new List<Reward>();

        [System.Serializable]
        public class Reward
        {
            [Min(1)]
            public int number;
            public InventoryItem item;
        }

        [System.Serializable]
        public class Objective
        {
            public string reference;
            public string description;
            public List<Reward> rewards;
            public List<InventoryItem> requiredItems;
        }

        public IEnumerable<string> GetTasks()
        {
            yield return "Task 1";
            Debug.Log("Do some work");
            yield return "Task 2";
        }

        public string GetTitle()
        {
            return name;
        }
        public int GetObjectiveCount()
        {
            return objectives.Count;
        }

        public IEnumerable<Objective> GetObjectives()
        {
            return objectives;
        }

        public IEnumerable<Reward> GetRewards()
        {
            return rewards;
        }
        public IEnumerable<Reward> GetObjectiveRewards(string objectiveRef)
        {
            foreach(var objective in objectives)
            {
                if(objective.reference == objectiveRef)
                {
                    return objective.rewards;
                }
            }
            return null;
        }
        public IEnumerable<InventoryItem> GetObjectiveRequiredItems(string objectiveRef)
        {
            foreach(var objective in objectives)
            {
                if(objective.reference == objectiveRef)
                {
                    return objective.requiredItems;
                }
            }
            return null;
        }

        public bool HasObjective(string objectiveRef)
        {
            foreach(var objective in objectives)
            {
                if(objective.reference == objectiveRef)
                {
                    return true;
                }
            }
            return false;
        }

        public static Quest GetByName(string questName)
        {
            foreach(Quest quest in Resources.LoadAll<Quest>(""))
            {
                if(quest.name == questName)
                {
                    return quest;
                }
            }
            return null;
        }
    }

}