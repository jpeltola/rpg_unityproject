﻿using RPG.Combat;
using RPG.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Quests
{
    // Holds the data for destroyed camps (for quest conditions)
    public class CampManager : MonoBehaviour,IPredicateEvaluator
    {
        [SerializeField] List<string> destroyedCamps;

        private void OnEnable()
        {
            foreach(var camp in FindObjectsOfType<AggroGroup>())
            {
                camp.onFighterDead.AddListener(CampDestroyed);
            }
        }

        private void CampDestroyed(string campID)
        {
            destroyedCamps.Add(campID);
        }

        public bool? Evaluate(EPredicate predicate, string[] parameters)
        {
            switch(predicate)
            {
                case EPredicate.IsCampCleared:
                    return destroyedCamps.Contains(parameters[0]);  
            }
            return null;
        }
    }

}