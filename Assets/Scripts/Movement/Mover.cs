﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RPG.Core;
using RPG.Saving;
using RPG.Attributes;
using RPG.Stats;

namespace RPG.Movement
{
    // Component that is responsible for the movement of a character
    public class Mover : MonoBehaviour, IAction, ISaveable
    {
        //[SerializeField] float maxSpeed = 6f;
        [SerializeField] float maxNavPathLength = 40f;

        NavMeshAgent navMeshAgent;
        Health health;
        Vector3 destination;
        bool hasDestination = false;
        float speedFraction = 1;
        [System.Serializable]
        struct MoverSaveData
        {
            public SerializableVector3 position;
            public SerializableVector3 rotation;
        }
        private void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            health = GetComponent<Health>();
        }
        // Update is called once per frame
        void Update()
        {
            navMeshAgent.enabled = !health.IsDead();
            UpdateAnimator();
            if(hasDestination && !health.IsDead())
            {
                MoveTo(destination, speedFraction);
            }
        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            MoveTo(destination,speedFraction);
            
        }

        public bool CanMoveTo(Vector3 destination)
        {
            NavMeshPath path = new NavMeshPath();
            bool hasPath = NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path);
            if (!hasPath) return false;
            if(path.status != NavMeshPathStatus.PathComplete)return false;
            if (GetPathLength(path) > maxNavPathLength) return false;

            return true;
        }

        public void MoveTo(Vector3 destination, float speedFraction)
        {
            if (!navMeshAgent.enabled && !gameObject.activeSelf && !navMeshAgent.isOnNavMesh) return;
            navMeshAgent.speed = GetMaxSpeed() * Mathf.Clamp01(speedFraction);
            navMeshAgent.destination = destination;
            navMeshAgent.isStopped = false;
            this.destination = destination;
            this.speedFraction = speedFraction;
            this.hasDestination = true;
        }

        public float GetMaxSpeed()
        {
            return Mathf.Max(0,GetComponent<BaseStats>().GetStat(Stat.MovementSpeed));
        }

        public void Cancel()
        {
            if (!navMeshAgent.enabled && !gameObject.activeSelf && !navMeshAgent.isOnNavMesh) return;
            navMeshAgent.isStopped = true;
            hasDestination = false;
        }

        private void UpdateAnimator()
        {
            Vector3 velocity = GetComponent<NavMeshAgent>().velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            GetComponent<Animator>().SetFloat("forwardSpeed", speed);
        }
        private float GetPathLength(NavMeshPath path)
        {
            float length = 0;
            if (path.corners.Length < 2) return length; // if less than 2 corners, no distance can be calculated
            for (int i = 1; i < path.corners.Length; i++)
            {
                length += Vector3.Distance(path.corners[i], path.corners[i - 1]);
            }
            return length;
        }

        public object CaptureState()
        {
            MoverSaveData data = new MoverSaveData();
            data.position = new SerializableVector3(transform.position);
            data.rotation = new SerializableVector3(transform.eulerAngles);
            return data;
        }

        public void RestoreState(object state)
        {
            MoverSaveData data = (MoverSaveData)state;
            navMeshAgent.Warp(data.position.ToVector3());
            transform.eulerAngles = data.rotation.ToVector3();
        }

    }

}