﻿

namespace RPG.Control
{
    // Interface for objects that can be clicked (mouse raycasting)
    public interface IRaycastable
    {
        CursorType GetCursorType(); // Return the cursor type that certain IRaycastable should show
        bool HandleRayCast(PlayerController callingController); // What to do when raycast hit is clicked
        

    }
}
