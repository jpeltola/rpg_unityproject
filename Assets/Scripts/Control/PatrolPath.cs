﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    // Hold data for the patrol points of the patrol path. Tells AI which is next point to be walked to
    public class PatrolPath : MonoBehaviour
    {
        enum PatrolRouteType
        {
            OneDirectional, TwoDirectional
        }

        [SerializeField] PatrolRouteType patrolRouteType = PatrolRouteType.OneDirectional;
        const float waypointGizmoRadius = 0.3f;
        bool forward = true;

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.position, 0.1f);
            Gizmos.color = Color.white;
            bool forward2 = forward; // for that this function wont change forward
            for (int i = 0; i < transform.childCount; i++)
            {
                int j = GetNextIndex(i);
                Gizmos.DrawSphere(GetWaypoint(i), waypointGizmoRadius);
                Gizmos.DrawLine(GetWaypoint(i), GetWaypoint(j));
            }
            forward = forward2; // for that this function wont change forward
        }
#endif
        public int GetNextIndex(int i)
        {

            switch(patrolRouteType)
            {
                case PatrolRouteType.OneDirectional:
                    return (i + 1) % transform.childCount;
                
                case PatrolRouteType.TwoDirectional:
          
                    if(i%transform.childCount == 0 )
                    {
                        forward = true;
                    }
                    else if((i + 1) % transform.childCount == 0)
                    {
                        forward = false;
                    }
                    int j = forward ? 1 : -1;
                    return i + j;
            }
            return 0;
        }

        public Vector3 GetWaypoint(int i)
        {
            return transform.GetChild(i).transform.position;
        }
    }
}
