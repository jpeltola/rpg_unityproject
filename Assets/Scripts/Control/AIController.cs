﻿using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Core;
using RPG.Attributes;
using System;
using GameDevTV.Utils;
using RPG.Saving;
using UnityEngine.AI;
using RPG.InGameTime;

namespace RPG.Control
{
    // Component responsible for enemy AI state: (patroling, fighting etc..)
    public class AIController : MonoBehaviour,ISaveable
    {
       
        [SerializeField] float chaseDistance = 10f;
        [SerializeField] float alertDistance = 6f;
        [SerializeField] float suspicionTime = 3f;
        
        [SerializeField] PatrolPath patrolPath;
        [SerializeField] float waypointTolerance = 1f;
        [SerializeField] float dwellTime = 3f;
        [Range(0, 1)]
        [SerializeField] float patrolSpeedFraction = 0.3f;

        [SerializeField] float aggroDistance = 6f;
        
        Fighter fighter;
        GameObject player;
        Health health;
        Mover mover;

        Vector3 guardPosition;

        float timeSinceLastSawPlayer = Mathf.Infinity;
        float timeSinceArrivedAtWaypoint = Mathf.Infinity;

        int currentWaypoint = 0;

        [System.Serializable]
        struct SaveData
        {
            public float timeSinceLastSawPlayer;
            public float timeSinceArrivedAtWaypoint;
            public int currentWayPoint;
        }

        private void Awake()
        {
            fighter = GetComponent<Fighter>();
            player = GameObject.FindWithTag("Player");
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();

            guardPosition = transform.position;

        }

        //This function calculates the AI position back if player stays long enough in the other level (so the enemy wont be at the transition point infinite amount of time)
        private void CalculatePathBack(float time)
        {

            if ( health.IsDead()  || timeSinceLastSawPlayer + time < suspicionTime)
            {
                return;
            }

            Vector3 destination = guardPosition;

            if (patrolPath != null)
            {
                destination = GetCurrentWayPoint();
            }

            NavMeshPath path = new NavMeshPath();
            NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path);
            float speed = mover.GetMaxSpeed() * patrolSpeedFraction;
            float distanceToTravel = speed * time;
            if(timeSinceLastSawPlayer < suspicionTime)
            {
                distanceToTravel -= speed * (suspicionTime - timeSinceLastSawPlayer);
            }


            for (int i = 1; i < path.corners.Length; i++)
            {
                float distance = Vector3.Distance(path.corners[i - 1], path.corners[i]);

                if (distance > distanceToTravel)
                {
                    Vector3 newPosition = (path.corners[i] - path.corners[i - 1]) * distanceToTravel / distance + path.corners[i - 1];
                    GetComponent<NavMeshAgent>().Warp(newPosition);
                    return;
                }
                distanceToTravel -= distance;
            }
            GetComponent<NavMeshAgent>().Warp(destination);

        }
        private void Start()
        {
            float time = FindObjectOfType<IngameTimeGlobal>().GetDeltaTime();
            CalculatePathBack(time);

            timeSinceLastSawPlayer += time;
            timeSinceArrivedAtWaypoint += time;
        }

        private void Update()
        {
            if (health.IsDead()) return;

            CheckAggro();

            if (fighter != null && IsAggrevated()  && fighter.CanAttack(player))
            {
                 AttackBehaviour();
            }
            else if (timeSinceLastSawPlayer < suspicionTime)
            {
                SuspicionBehaviour();
            }
            else
            {
                PatrolBehaviour();
            }
            
            UpdateTimers();
        }

        public void Aggrevate()
        {
            timeSinceLastSawPlayer = 0;
            if (!IsAggrevated())
            {
                AggrevateNearbyEnemies();
            }
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPosition = guardPosition;
            if(patrolPath != null)
            {
                if(AtWayPoint())
                {
                    timeSinceArrivedAtWaypoint = 0;
                    CycleWaypoint();
                }
                nextPosition = GetCurrentWayPoint();
  
            }
            if (timeSinceArrivedAtWaypoint > dwellTime)
            {
                mover.StartMoveAction(nextPosition,patrolSpeedFraction);
            }
        }

        private Vector3 GetCurrentWayPoint()
        {
            return patrolPath.GetWaypoint(currentWaypoint);
        }
        
        private void CycleWaypoint()
        {
            currentWaypoint = patrolPath.GetNextIndex(currentWaypoint);
        }

        private bool AtWayPoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWayPoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void SuspicionBehaviour()
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AttackBehaviour()
        {
            fighter.Attack(player);
            AggrevateNearbyEnemies();
        }

        private void AggrevateNearbyEnemies()
        {
            if (!GetComponent<Fighter>().enabled) return;
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, alertDistance, Vector3.up, 0);

            foreach(RaycastHit hit in hits)
            {
                AIController ai = hit.collider.GetComponent<AIController>();
                if (ai == null || ai == this || !IsInChaseDistance() || (ai.IsAggrevated() && ai.IsInChaseDistance()) ) continue;
                ai.Aggrevate();
                
                if(ai.GetComponent<Fighter>() != null)
                {
                    ai.GetComponent<Fighter>().enabled = true;
                }  
            }
        }
        // Check if should aggro the player
        private void CheckAggro()
        {
            if (fighter == null) return;
            if (IsInAggroDistance() && fighter.CanSeeTarget(player.transform))
            {
                Aggrevate();
            }
            if(IsAggrevated() && IsInChaseDistance() && fighter.CanSeeTarget(player.transform) )
            {
                Aggrevate();
            }
        }
        private bool IsAggrevated()
        {
            return timeSinceLastSawPlayer < suspicionTime;
        }
        private bool IsInAggroDistance()
        {
            return Vector3.Distance(player.transform.position, transform.position) < aggroDistance;
        }
        private bool IsInChaseDistance()
        {
            return Vector3.Distance(player.transform.position, transform.position) < chaseDistance;
        }

        private void UpdateTimers()
        {
            timeSinceLastSawPlayer += Time.deltaTime;
            timeSinceArrivedAtWaypoint += Time.deltaTime;
        }

        //Called by Unity
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, aggroDistance);
            Gizmos.DrawWireSphere(transform.position, chaseDistance);

        }
        // Save functions
        public object CaptureState()
        {
            SaveData data = new SaveData();
            data.currentWayPoint = currentWaypoint;
            data.timeSinceArrivedAtWaypoint = timeSinceArrivedAtWaypoint;
            data.timeSinceLastSawPlayer = timeSinceLastSawPlayer;
            data.currentWayPoint = currentWaypoint;
            return data;
        }
        // Save functions
        public void RestoreState(object state)
        {
            SaveData data = (SaveData)state;
            currentWaypoint = data.currentWayPoint;
            timeSinceArrivedAtWaypoint = data.timeSinceArrivedAtWaypoint;
            timeSinceLastSawPlayer = data.timeSinceLastSawPlayer;
            currentWaypoint = data.currentWayPoint;
        }
    }
}
