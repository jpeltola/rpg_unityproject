﻿using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Attributes;
using System;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using RPG.Stats;
using RPG.Inventories;

namespace RPG.Control
{
    // Responsible for player input
    public class PlayerController : MonoBehaviour
    {
        Health health;

        [System.Serializable]
        struct CursorMapping
        {
            public CursorType type;
            public Texture2D texture;
            public Vector2 hotspot;
        }

        [SerializeField] CursorMapping[] cursorMappings = null;
        [SerializeField] float maxNavMeshProjectionDistance = 1f;
        [SerializeField] float raycastRadius = 0.1f;

        FollowCamera followCamera;

        bool isDraggingUI = false;
        private void Awake()
        {
            health = GetComponent<Health>();
        }
        private void Start()
        {
            followCamera = FindObjectOfType<FollowCamera>();
        }

        private void Update()
        {

            if (InteractWithUI()) return;
            CheatInput();
            if (health.IsDead()) return;

            HandleCameraControls();
            HandleActionInput();

            if (InteractWithComponent()) return;

            if (InteractWithMovement()) return;
            SetCursor(CursorType.None);

        }

        private void CheatInput()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                health.Revive();
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                GetComponent<Experience>().GainExperience(30);
            }
            
        }

        private void HandleCameraControls()
        {
            if (Input.GetKey(KeyCode.D))
            {
                followCamera.RotateHorizontal(-1);
            }
            if (Input.GetKey(KeyCode.A))
            {
                followCamera.RotateHorizontal(1);
            }
            if (Input.GetKey(KeyCode.W))
            {
                followCamera.RotateVertical(1);
            }
            if (Input.GetKey(KeyCode.S))
            {
                followCamera.RotateVertical(-1);
            }
            if (Input.mouseScrollDelta.y != 0)
            {
                followCamera.Zoom(Input.mouseScrollDelta.y);
            }
            
        }
        private void HandleActionInput()
        {
            ActionStore actionStore = GetComponent<ActionStore>();
            for (int i = (int)KeyCode.Alpha1; i <= (int)KeyCode.Alpha6; i++)
            {
                if (Input.GetKeyDown((KeyCode)i) )
                {
                    GameObject target = null;

                    if(GetComponent<Fighter>().GetTarget() != null)
                    {
                        target = GetComponent<Fighter>().GetTarget().gameObject;
                    }
                    actionStore.Use(i - (int)KeyCode.Alpha1, gameObject, target);
                }   
            }

        }

        private bool InteractWithUI()
        {
            if (Input.GetMouseButtonUp(0))
            {
                isDraggingUI = false;
            }
            if (EventSystem.current.IsPointerOverGameObject())
            {
                if (Input.GetMouseButtonDown(0))
                {
                    isDraggingUI = true;
                }
                SetCursor(CursorType.UI);
                return true;
            }
            return isDraggingUI;
        }

        private bool InteractWithComponent()
        {
            RaycastHit[] hits = RaycastAllSorted();
            foreach (RaycastHit hit in hits)
            {
                IRaycastable[] raycastables= hit.transform.GetComponents<IRaycastable>();
                foreach(IRaycastable raycastable in raycastables)
                {
                    if(raycastable.HandleRayCast(this))
                    {
                        SetCursor(raycastable.GetCursorType());
                        return true;
                    }
                }
            }
            return false;
        }

        private RaycastHit[] RaycastAllSorted()
        {
            //Get all hits
            RaycastHit[] hits = Physics.SphereCastAll(GetMouseRay(),raycastRadius);
            //sort by distance
            //build array distances
            float[] distances = new float[hits.Length];
            for(int i = 0; i< distances.Length;i++)
            {
                distances[i] = hits[i].distance;
            }
            Array.Sort(distances, hits);
            return hits;
        }

        private bool InteractWithMovement()
        {
            bool hasHit = RaycastNavMesh(out Vector3 target);
            if (hasHit)
            {
                
                if (!GetComponent<Mover>().CanMoveTo(target)) return false;
                SetCursor(CursorType.Movement);
                if (Input.GetMouseButton(0))
                {
                    SetCursor(CursorType.MovementStarted);
                    GetComponent<Mover>().StartMoveAction(target,1);
                }
                
                return true;
            }
            return false;
        }
        private bool RaycastNavMesh(out Vector3 target)
        {
            target = new Vector3();
            // Raycast to terrain
 
            bool didHit = Physics.Raycast(GetMouseRay(),out RaycastHit hit);
            if (!didHit) return false;
            // Find nearest navmesh point
            
            
            bool hasFoundNearest = NavMesh.SamplePosition(hit.point, out NavMeshHit navMeshHit, maxNavMeshProjectionDistance, NavMesh.AllAreas);
            // return true if found
            if (!hasFoundNearest) return false;
            target = navMeshHit.position;

            return true;
        }

        private void SetCursor(CursorType type)
        {
            CursorMapping mapping = GetCursorMapping(type);
            Cursor.SetCursor(mapping.texture, mapping.hotspot, CursorMode.Auto);
        }

        private CursorMapping GetCursorMapping(CursorType type)
        {
            foreach(CursorMapping mapping in cursorMappings)
            {
                if(mapping.type == type)
                {
                    return mapping;
                }
            }
            return new CursorMapping();
        }

        private Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }

    }
}

