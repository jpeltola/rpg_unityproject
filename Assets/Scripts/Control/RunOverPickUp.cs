﻿using RPG.Inventories;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Movement;
using RPG.Core;

namespace RPG.Control
{
    // Raycastable object for pickup items.
    [RequireComponent(typeof(Pickup))]
    public class RunOverPickUp : MonoBehaviour, IRaycastable,IAction
    {
        Pickup pickup;
        bool isPickingUp = false;
        private void Awake()
        {
            pickup = GetComponent<Pickup>();
        }


        public CursorType GetCursorType()
        {
            if (pickup.CanBePickedUp())
            {
                return CursorType.Pickup;
            }
            else
            {
                return CursorType.FullPickup;
            }
        }

        // When object is clicked, move the player towards the item
        public bool HandleRayCast(PlayerController callingController)
        {
            if(Input.GetMouseButtonDown(0))
            {
                callingController.GetComponent<ActionScheduler>().StartAction(this);
                isPickingUp = true;
                callingController.GetComponent<Mover>().MoveTo(transform.position, 1f);
            }
            return true;
        }

        // When player overlaps the item collider, pick up the item if pickup action is active.
        private void OnTriggerStay(Collider other)
        {
            if (isPickingUp && other.gameObject.CompareTag("Player") )
            {
                GetComponent<Pickup>().PickupItem();
                other.GetComponent<ActionScheduler>().CancelCurrentAction();
            }
        }
        // Cancel pickup action (is cancelled automatically through ActionScheluder when starts a move action or fighting) 
        public void Cancel()
        {
            isPickingUp = false;
        }
    }

}