﻿
namespace RPG.Control
{
    // List of different cursor types
    public enum CursorType
    {
        None,
        Movement,
        Attack,
        UI,
        Pickup,
        Dialogue,
        FullPickup,
        MovementStarted
    }
}