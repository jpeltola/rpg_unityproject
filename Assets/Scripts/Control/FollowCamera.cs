﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    // Camera, that follows the player and rotates around the player 
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] float rotationXSpeed = 120f;
        [SerializeField] float rotationYSpeed = 30f;
        [SerializeField] float zoomSpeed = 15f;
        [SerializeField] float maxDistance = 15f;
        [SerializeField] float minDistance = 6f;
        [Range(0, 1)]
        [SerializeField] float startXRotation = 0.5f;
        [SerializeField] Camera cameraObject = null;

        private float maxXRotation = 80f;
        private float minXRotation = 10f;
   

        private void Awake()
        {
            cameraObject.transform.localPosition = new Vector3(0, 0, -maxDistance);
            cameraObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
            Vector3 euler = transform.localRotation.eulerAngles;
            transform.Rotate(new Vector3(-euler.x, 0, 0));
            transform.Rotate(new Vector3(0, 0, -euler.z));
            transform.Rotate(startXRotation * (maxXRotation - minXRotation) + minXRotation, 0, 0);
        }

        public void Zoom(float direction)
        {
            float amount = direction * zoomSpeed*Time.deltaTime;
            float currentDistance = cameraObject.transform.localPosition.z;

            //Clamp to desired distances
            float newDistance = Mathf.Clamp(currentDistance + amount, -maxDistance, -minDistance);
            cameraObject.transform.localPosition = new Vector3(0, 0, newDistance);
        }
        public void RotateVertical(float direction)
        {

            transform.Rotate(direction * rotationYSpeed * Time.deltaTime, 0,0);

            //Clamp to desired angles
            Vector3 currentRotation = transform.localRotation.eulerAngles;
            currentRotation.x = Mathf.Clamp(currentRotation.x, minXRotation, maxXRotation);
            transform.localRotation = Quaternion.Euler(currentRotation);
        }

        public void RotateHorizontal(float direction)
        {
            transform.Rotate(0, direction* rotationXSpeed * Time.deltaTime, 0, Space.World);
        }

        void LateUpdate()
        {
            transform.position = target.position;
        }
    }

}