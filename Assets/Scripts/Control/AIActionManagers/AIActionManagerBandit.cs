﻿using RPG.Attributes;
using RPG.Combat;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    // Action manager for bandits (uses Rally and health potion.)
    public class AIActionManagerBandit : AIActionManager
    {
        Fighter fighter;
        Health health;

        private void Awake()
        {
            fighter = GetComponent<Fighter>();
            health = GetComponent<Health>();
        }

        private void Update()
        {
            // Whenever Bandit has target, try use Rally (if not on cooldown)
            if (fighter.GetTarget() != null)
            {
                UseAction(0, gameObject);
            }
            // When health below 20 %, use health potion.
            if(health.GetPercentage() < 20f)
            {
                UseAction(1,gameObject);
            }
        }
    }
}