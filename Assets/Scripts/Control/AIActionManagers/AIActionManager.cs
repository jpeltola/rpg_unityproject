﻿using RPG.Combat;
using RPG.Inventories;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    // Manages the Enemies action usage. Inherit all action managers from this for each enemy types that uses actions
    public abstract class AIActionManager : MonoBehaviour
    {
        [SerializeField] ActionItem[] actions;
   
        public void UseAction(int actionIndex, GameObject target)
        {
            actions[actionIndex].Use(gameObject,target);
        }
    }
}
