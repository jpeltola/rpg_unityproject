﻿using RPG.Combat;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Attributes
{

    // UI element for showing the health
    public class EnemyHealthDisplay : MonoBehaviour
    {
        Fighter fighter;

        private void Awake()
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
        }
        private void Update()
        {
            Health health = fighter.GetTarget();
            if (health == null)
            {
                GetComponent<Text>().text = "N/A";
                return;
            }
            

            GetComponent<Text>().text = String.Format("{0:0}/{1:0}", Mathf.Ceil(health.GetCurrentHealthPoints()), Mathf.Ceil(health.GetMaxHealthPoints()));
        }
    }

}