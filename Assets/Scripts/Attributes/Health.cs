﻿using GameDevTV.Utils;
using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Attributes
{
    // Health component: responsible for characters current health, taking damage and checking if alive.
    public class Health : MonoBehaviour, ISaveable, IPredicateEvaluator
    {
        [Range(0,1)]
        [SerializeField] float regeneratePercentage = 0.7f;
        [SerializeField] public TakeDamageEvent onTakeDamage;
        [SerializeField] public UnityEvent onDie;
        [SerializeField] public UnityEvent onRevive;
        [SerializeField] public TakeDamageEvent onHealed;

        [System.Serializable]
        struct SaveData
        {
            public float healthPoints;
        }

        float regenTimer = 0;

        [System.Serializable]
        public class TakeDamageEvent : UnityEvent<float>
        {
        }

        LazyValue<float> healthPoints;
        private bool isDead = false;

        private void Update()
        {
            Regenerate();
        }

        // Health regen function
        private void Regenerate()
        {
            //Update health once per second
            if (regenTimer > 1 && !IsDead() && GetCurrentHealthPoints() < GetMaxHealthPoints())
            {
                float healthRegen = GetComponent<BaseStats>().GetStat(Stat.HealthRegeneration);
                if (healthRegen < 0)
                {
                    TakeDamage(gameObject, -healthRegen);
                }
                else
                {
                    healthPoints.value = Mathf.Min(healthPoints.value + healthRegen, GetMaxHealthPoints());
                }

                regenTimer = 0;
            }
            regenTimer += Time.deltaTime;
        }

        private void Awake()
        {
            healthPoints = new LazyValue<float>(GetInitialHealth);
        }
        private void Start()
        {
            healthPoints.ForceInit();
        }

        private float GetInitialHealth()
        {
            return GetComponent<BaseStats>().GetStat(Stat.Health);
        }
        private void OnEnable()
        {
            GetComponent<BaseStats>().onLevelUp.AddListener(RegenerateHealth);
        }
        private void OnDisable()
        {
            GetComponent<BaseStats>().onLevelUp.AddListener(RegenerateHealth);
        }
        public bool IsDead()
        {
            return isDead;
        }

        public float TakeDamage(GameObject instigator, float damage)
        {
            if (IsDead()) return 0;
            float damageTaken = Mathf.Max(healthPoints.value - damage, 0);
            healthPoints.value = damageTaken;
            if(healthPoints.value == 0 )
            {
                onDie?.Invoke();
                Die();
                AwardExperience(instigator);
            }
            else
            {
                onTakeDamage.Invoke(damage);
            }
            return damageTaken;
        }
        public float GetPercentage()
        {
            return 100*GetFraction();
        }
        public float GetFraction()
        {
            return healthPoints.value / GetComponent<BaseStats>().GetStat(Stat.Health);
        }
        public float GetCurrentHealthPoints()
        {
            return healthPoints.value;
        }
        public float GetMaxHealthPoints()
        {
            return GetComponent<BaseStats>().GetStat(Stat.Health);
        }

        private void Die()
        {
            if (isDead) return;
            isDead = true;
            GetComponent<Collider>().enabled = false;
            GetComponent<Animator>().SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }
        public void Revive()
        {
            if (!isDead) return;
            isDead = false;
            healthPoints.value = GetMaxHealthPoints();
            GetComponent<Collider>().enabled = true;
            GetComponent<Animator>().SetTrigger("revive");
        }

        public void Heal(float amount)
        {
            healthPoints.value = Mathf.Min(healthPoints.value + amount, GetMaxHealthPoints());
            onHealed.Invoke(amount);
        }

        // When enemy is dead, award experience to the player
        private void AwardExperience(GameObject instigator)
        {
            Experience experience = instigator.GetComponent<Experience>();
            if (experience == null) return;
            experience.GainExperience(GetComponent<BaseStats>().GetStat(Stat.Experience));
        }

        // Regen health when level up
        private void RegenerateHealth()
        {
            float regenHealthPoints = GetComponent<BaseStats>().GetStat(Stat.Health)*regeneratePercentage;
            healthPoints.value = Mathf.Max(healthPoints.value, regenHealthPoints);
        }


        // Saving functions
        public object CaptureState()
        {
            SaveData data = new SaveData();
            data.healthPoints = healthPoints.value;
            return data;
        }

        public void RestoreState(object state)
        {
            SaveData data = (SaveData)state;
            healthPoints.value = data.healthPoints;
            
            if (healthPoints.value == 0)
            {
                isDead = true;
                GetComponent<Collider>().enabled = false;
                GetComponent<ActionScheduler>().CancelCurrentAction();
                GetComponent<Animator>().Play("Death", -1, 1);
            }
            else
            {
                isDead = false;
                GetComponent<Collider>().enabled = true;
                GetComponent<Animator>().Play("Locomotion"); 
            }

            
        }

        // Predicate evaluation for condition checking
        public bool? Evaluate(EPredicate predicate, string[] parameters)
        {
            switch(predicate)
            {
                case EPredicate.IsDead:
                    return IsDead();
            }
            return null;
        }
    }
}
