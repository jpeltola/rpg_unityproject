﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Attributes
{
    // UI component for showing healthbar over enemies
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] RectTransform foreground = null;
        [SerializeField] Canvas canvas = null;
        [SerializeField] Health health = null;
        [SerializeField] float timeVisible = 6.0f;


        float timer = Mathf.Infinity;


        private void OnDisable()
        {
            canvas.enabled = false;
        }

        public void Update()
        {
            if (health == null) return;
            foreground.localScale = new Vector3(health.GetFraction(), 1, 1);

            canvas.enabled = timer < timeVisible && !health.IsDead();
            timer += Time.deltaTime;
        }

        public void HandleOnTakeDamage(float damageTaken)
        {
            if(damageTaken > 0)
            {
                timer = 0;
            }  
        }

    }

}