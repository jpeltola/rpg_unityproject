﻿using RPG.Stats;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Attributes
{
    // A Buffeffect that scales based on missing health.
    public class Berserk : BuffEffect,IModifierProvider

    {
        public override IEnumerable<float> GetAdditiveModifiers(Stat stat)
        {
            if(stat == GetStat())
            {
                yield return (100 - GetComponent<Health>().GetPercentage()) / 100 * GetAdditiveAmount();
                
            }
            yield return 0;
        }

        public override IEnumerable<float> GetPercentageModifiers(Stat stat)
        {
            if (stat == GetStat())
            {
                yield return (100 - GetComponent<Health>().GetPercentage())/100 * GetMultiplicativeAmount();
            }
            yield return 0;
        }

    }
}
