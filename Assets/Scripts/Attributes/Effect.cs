﻿using RPG.Combat;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Attributes
{
    // EffectData is a data class for constructing the Effect class
    [System.Serializable]
    public class EffectData
    {
        public EffectType type;
        public Stat stat;
        public float additiveAmount = 0;
        public float multiplicativeAmount = 0;
        public float duration = 0;
        public bool doesStack = false;
    }

    // enum for different Effect types
    public enum EffectType
    {
        Poison,
        Buff
    }

    // Effect that increases or decreases temporarily some stats
    [System.Serializable]
    public abstract class Effect : MonoBehaviour
    {
        GameObject instigator;
        [SerializeField] float additiveAmount;
        [SerializeField] float multiplicativeAmount;
        [SerializeField] float duration;
        [SerializeField] Stat stat;
        [SerializeField] bool doesStack;

        float timeAffected = 0;

        protected virtual void Update()
        {
            //Destroy effect after certain duration
            if(timeAffected > duration)
            {
                Destroy(this);
            }
            timeAffected += Time.deltaTime;
        }

        // A helper function to get the actual type of the Effect based on input enum. 
        static public Type GetType(EffectType type)
        {
            switch (type)
            {
                case EffectType.Buff:
                    return typeof(BuffEffect);
                case EffectType.Poison:
                    return typeof(Poison);
            }
            return null;
        }

        public void Setup(EffectData effectData, GameObject instigator)
        {
            this.instigator = instigator;
            this.additiveAmount = effectData.additiveAmount;
            this.multiplicativeAmount = effectData.multiplicativeAmount;
            this.duration = effectData.duration;
            this.stat = effectData.stat;
            this.doesStack = effectData.doesStack;
        }

        public GameObject GetInstigator()
        {
            return instigator;
        }

        public float GetDuration()
        {
            return duration;
        }
        public float GetAdditiveAmount()
        {
            return additiveAmount;
        }
        public float GetMultiplicativeAmount()
        {
            return multiplicativeAmount;
        }
        public Stat GetStat()
        {
            return stat;
        }

        public bool DoesStack()
        {
            return doesStack;
        }

        public virtual bool CanHaveMultipleInstances()
        {
            return true;
        }

    }
}