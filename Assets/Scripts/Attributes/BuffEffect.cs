﻿using RPG.Attributes;
using RPG.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Stats
{

    // Effect that apply buff/debuff
    [System.Serializable]
    public class BuffEffect : Effect, IModifierProvider
    {
        public virtual IEnumerable<float> GetAdditiveModifiers(Stat stat)
        {
            
            if (stat == GetStat())
            {
                yield return GetAdditiveAmount();
            }
            yield return 0;
        }

        public virtual IEnumerable<float> GetPercentageModifiers(Stat stat)
        {  
            if (stat == GetStat())
            {
                yield return GetMultiplicativeAmount();
            }

            yield return 0;
        }
        public override bool CanHaveMultipleInstances()
        {
            return true;
        }

        protected override void Update()
        {
            base.Update();   
        }

    }
}