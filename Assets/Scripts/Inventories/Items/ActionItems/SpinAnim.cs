﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// added to spinning axe prefab to visually show the spin action
public class SpinAnim : MonoBehaviour
{

    float angle = 0;
    float rotationSpeed = 900;
    [SerializeField] Transform axe;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
        angle += rotationSpeed * Time.deltaTime;
        if (angle > 360) Destroy(gameObject);
    }

    public void Setup(float range)
    {
        axe.localScale = new Vector3(1, 1, 1)* 1.5f * range/3.2f;
    }
}
