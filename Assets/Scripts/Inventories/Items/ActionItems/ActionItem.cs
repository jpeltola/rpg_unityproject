﻿using RPG.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Inventories
{
    // An item that can be moved to an action slot and used (potions, spells etc..)
    public abstract class ActionItem : InventoryItem
    {
        [SerializeField] bool isConsumable = true;
        [SerializeField] protected float cooldownTime = 20;
        [SerializeField] string token;
        [SerializeField] protected AudioClip soundEffect;
        [SerializeField] Condition conditionsOnUser;
        [SerializeField] Condition conditionsOnTarget;

        [SerializeField] bool onSelf = true;

        public ActionItemEvent onUsed;

        public class ActionItemEvent : UnityEvent<ActionItem>
        {
        }

        public bool IsConsumable()
        {
            return isConsumable;
        }
        ///<summary>
        ///Uses the action item. Return true if successful
        ///<summary>
        public virtual bool Use(GameObject user, GameObject target)
        {
            return true;
        }
        public void SetCooldown(GameObject user)
        {
            user.GetComponent<CooldownManager>().SetCooldownTimer(GetToken(), cooldownTime);
        }

        public string GetToken()
        {
            return token;
        }

        protected virtual bool CanUse(GameObject user, GameObject target)
        { 
            if(target == null)
            {
                return conditionsOnUser.Check(user.GetComponents<IPredicateEvaluator>()) && conditionsOnTarget.Check(null);
            }
            return conditionsOnUser.Check(user.GetComponents<IPredicateEvaluator>()) && conditionsOnTarget.Check(target.GetComponents<IPredicateEvaluator>());
        }

        public float CooldownRemaining(GameObject user)
        {
            return user.GetComponent<CooldownManager>().GetCooldown(GetToken());
        }
        public float GetCooldown()
        {
            return cooldownTime;
        }

        protected void PlaySoundEffect(Vector3 position)
        {
            if (soundEffect != null)
            {
                AudioSource.PlayClipAtPoint(soundEffect,position);
            }
        }
        
        public bool IsOnSelf()
        {
            return onSelf;
        }
    }
}
