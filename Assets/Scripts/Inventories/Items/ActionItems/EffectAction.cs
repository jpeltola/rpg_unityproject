﻿using RPG.Attributes;
using RPG.Combat;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Inventories
{
    // An action item that on use, adds an effect to target
    [CreateAssetMenu(menuName = ("Inventory/ActionItem/Buff"))]
    public class EffectAction : ActionItem
    {
        [SerializeField] EffectData[] effectsAdded;

        public override bool Use(GameObject user, GameObject target)
        {
            if (CanUse(user, target))
            {
                if (IsOnSelf()) target = user;
                SetCooldown(user);
                foreach (var effect in effectsAdded)
                {
                    Type type = Effect.GetType(effect.type);
                    if(type != null)
                    {
                        Effect buff = target.gameObject.AddComponent(type) as Effect;
                        buff.Setup(effect, user);
                    }
                }
                onUsed?.Invoke(this);
                PlaySoundEffect(target.transform.position);
                return true;
            }
            return false;
        }
    }
}