﻿using RPG.Attributes;
using RPG.Combat;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{
    // Action item that applies a WeaponModifier to current weapon
    [CreateAssetMenu(menuName = ("Inventory/ActionItem/WeaponModifier"))]
    public class WeaponModificationAction : ActionItem
    {
        [SerializeField] WeaponModifier modifierPrefab;

        public override bool Use(GameObject user, GameObject target)
        {
            if (CanUse(user,target))
            {  
                SetCooldown(user);
                Fighter fighter = user.GetComponent<Fighter>();
                WeaponModifier modifier = fighter.GetWeapon().gameObject.AddComponent(modifierPrefab.GetType()) as WeaponModifier;
                modifier.Setup(modifierPrefab);
                return true;
            }
            return false;
        }
    }
}
