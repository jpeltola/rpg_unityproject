﻿using RPG.Attributes;
using RPG.Combat;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{
    // Action item that can be used when user wears an axe. Deals damage to nearby enemies
    [CreateAssetMenu(menuName = ("Inventory/ActionItem/Spin"))]
    public class Spin : ActionItem
    {
        [SerializeField] SpinAnim axePrefab;
        public override bool Use(GameObject user, GameObject target)
        {
            if(CanUse(user,target))
            {
                SetCooldown(user);
                float range = user.GetComponent<Fighter>().GetWeaponConfig().GetRange();

                RaycastHit[] hits = Physics.SphereCastAll(user.transform.position, range, Vector3.up, 0);
                foreach(var hit in hits)
                {
                    CombatTarget hitTarget = hit.collider.GetComponent<CombatTarget>();
                    if(hitTarget != null && hitTarget != user.GetComponent<CombatTarget>())
                    {
                        hitTarget.GetComponent<Health>().TakeDamage(user, user.GetComponent<BaseStats>().GetStat(Stat.Damage));
                    }
                }

                SpinAnim animInstance =  Instantiate(axePrefab, user.transform);
                animInstance.Setup(range);

                PlaySoundEffect(user.transform.position);
                return true;
            }
            return false;
        }
    }
}
