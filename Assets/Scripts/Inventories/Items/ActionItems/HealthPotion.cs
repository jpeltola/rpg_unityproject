﻿using RPG.Attributes;
using RPG.Combat;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Inventories
{
    // Action item that heals user
    [CreateAssetMenu(menuName = ("Inventory/ActionItem/Health Potion"))]
    public class HealthPotion : ActionItem
    {
        [SerializeField] float health = 20;
        [SerializeField] GameObject healEffect;

        public override bool Use(GameObject user, GameObject target)
        {
            if(CanUse(user, target))
            {
                SetCooldown(user);
                var healthComponent = user.GetComponent<Health>();
                if (healthComponent == null) return false;
                healthComponent.Heal(health);
                Instantiate(healEffect, user.transform);
                onUsed?.Invoke(this);
                PlaySoundEffect(user.transform.position);
                return true;
            }
            return false;
        }

        //protected override bool CanUse(GameObject user, CombatTarget target)
        //{
        //    return CooldownRemaining(user) <= 0 && !user.GetComponent<Health>().IsDead();
        //}
    }
}