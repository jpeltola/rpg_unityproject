﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    // Manages the cooldown of the actionitems
    public class CooldownManager : MonoBehaviour, IPredicateEvaluator
    {
        List<string> tokens = new List<string>();
        List<float> cooldownTimers = new List<float>();

        public Action onCooldownAdded;

        private void Update()
        {

            for (int i = 0; i < cooldownTimers.Count; i++)
            {
                cooldownTimers[i] -= Time.deltaTime;
                if (cooldownTimers[i] < 0)
                {
                    cooldownTimers.RemoveAt(i);
                    tokens.RemoveAt(i);
                    i -= 1;
                }
            }
        }

        // if no token is set, the item is never used -> cooldown 0
        public float GetCooldown(string token)
        {
            for (int i = 0; i < cooldownTimers.Count; i++)
            {
                if (tokens[i] == token)
                {
                    return cooldownTimers[i];
                }
            }
            return 0;
        }
        public bool IsOnCooldown(string token)
        {
            return GetCooldown(token) > 0;
        }


        //Adds a cooldowntimer to new token (item)
        public void SetCooldownTimer(string token, float cooldown)
        {
            if (tokens.Contains(token))
            {
                for (int i = 0; i < tokens.Count; i++)
                {
                    if (token == tokens[i])
                    {
                        cooldownTimers[i] = cooldown;
                        onCooldownAdded?.Invoke();
                        return;
                    }
                }
            }
            tokens.Add(token);
            cooldownTimers.Add(cooldown);

            onCooldownAdded?.Invoke();
        }

        public bool? Evaluate(EPredicate predicate, string[] parameters)
        {
            switch (predicate)
            {
                case EPredicate.ActionOnCooldown:
                    return IsOnCooldown(parameters[0]);
            }
            return null;
        }
    }
}
