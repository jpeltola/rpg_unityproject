﻿using RPG.Stats;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{
    // Equipable item that has statmodifiers 
    [CreateAssetMenu(menuName =("Inventory/Equipment"))]
    public class StatsEquipableItem : EquipableItem,IModifierProvider
    {
        [SerializeField] Modifier[] additiveModifiers;
        [SerializeField] Modifier[] multiplicativeModifiers;

        [System.Serializable]
        struct Modifier
        {
            public Stat stat;
            public float value;
        }
        public IEnumerable<float> GetAdditiveModifiers(Stat stat)
        {
            foreach(var modifier in additiveModifiers)
            {
                if(stat == modifier.stat)
                {
                    yield return modifier.value;
                }
            }
        }

        public IEnumerable<float> GetPercentageModifiers(Stat stat)
        {
            foreach (var modifier in multiplicativeModifiers)
            {
                if (stat == modifier.stat)
                {
                    yield return modifier.value;
                }
            }
        }
    }
}