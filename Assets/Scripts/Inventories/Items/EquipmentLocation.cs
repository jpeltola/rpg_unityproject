﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{
    // List of different equipment locations
    public enum EquipmentLocation
    {
        Helmet,
        Necklace,
        Armor,
        Trousers,
        Boots,
        Weapon,
        Shield,
        Gloves
    }
}
