﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{
   // Class for items that can be equipped
    public class EquipableItem : InventoryItem
    {
        [SerializeField] EquipmentLocation equipmentType;
        [SerializeField] int levelRequired;
        public EquipmentLocation GetEquipmentLocation()
        {
            return equipmentType;
        }

        public int GetLevelRequired()
        {
            return levelRequired;
        }
    }
}
