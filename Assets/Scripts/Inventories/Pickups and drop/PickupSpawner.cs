﻿using UnityEngine;
using RPG.Saving;

namespace RPG.Inventories
{
    /// <summary>
    /// Spawns pickups that should exist on first load in a level. This
    /// automatically spawns the correct prefab for a given inventory item.
    /// </summary>
    public class PickupSpawner : MonoBehaviour, ISaveable
    {
        // CONFIG DATA
        [SerializeField] Pickup defaultPickupPrefab = null;
        [SerializeField] InventoryItem item = null;
        [SerializeField] int number = 1;

        bool isCollected = false;
        // LIFECYCLE METHODS
        private void Awake()
        {
            // Spawn in Awake so can be destroyed by save system after.
            SpawnPickup();
        }

        // PUBLIC

        /// <summary>
        /// Returns the pickup spawned by this class if it exists.
        /// </summary>
        /// <returns>Returns null if the pickup has been collected.</returns>
        public Pickup GetPickup() 
        { 
            return GetComponentInChildren<Pickup>();
        }

        /// <summary>
        /// True if the pickup was collected.
        /// </summary>
        public bool IsCollected() 
        { 
            return isCollected;
        }

        //PRIVATE

        private void SpawnPickup()
        {
            var pickup = item.SpawnPickup(transform.position,number,defaultPickupPrefab);
            if (pickup == null) return;
 
            pickup.transform.SetParent(transform);
            pickup.onPickedUp += PickedUp;
        }

        private void PickedUp()
        {
            isCollected = true;
        }

        private void DestroyPickup()
        {
            if (GetPickup())
            {
                Destroy(GetPickup().gameObject);
            }
        }

        object ISaveable.CaptureState()
        {
            return isCollected;
        }

        void ISaveable.RestoreState(object state)
        {
            isCollected = (bool)state;
            if ( isCollected && GetComponentInChildren<Pickup>() != null)
            {
                DestroyPickup();
            }

            if (!isCollected && GetComponentInChildren<Pickup>() == null)
            {
                SpawnPickup();
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, 0.2f);
        }
    }
}