﻿using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.Inventories
{
    // Holds the data for possible item drops and their probabilities for certain enemies
    [CreateAssetMenu(menuName = ("Inventory/Drop Library"))]
    public class DropLibrary : ScriptableObject
    {
        [SerializeField] DropConfig[] potentialDrops;
        [System.Serializable]
        class DropConfig
        {
            public InventoryItem item;
            public ProgressionStatFormula dropChance;
            public ProgressionStatFormula minItems;
            public ProgressionStatFormula maxItems;
            public int GetRandomNumber(int level)
            {
                if(!item.IsStackable())
                {
                    return 1;
                }
                int min = (int)Mathf.Floor(minItems.Calculate(level));
                int max = (int)Mathf.Floor(maxItems.Calculate(level));
                return Random.Range(min, max + 1);
            }
        }

        public struct Dropped
        {
            public InventoryItem item;
            public int number;
        }


        public IEnumerable<Dropped> GetRandomDrops(int level)
        {
            for(int i = 0; i < potentialDrops.Length ; i++)
            {
                DropConfig dropConfig = potentialDrops[i];
                float roll = Random.Range(0f, 100f);
                if (dropConfig.dropChance.Calculate(level) > roll)
                {
                    Dropped drop = new Dropped();
                    drop.item = dropConfig.item;
                    drop.number = dropConfig.GetRandomNumber(level);
                    yield return drop;
                }
            }
        }
    }
}