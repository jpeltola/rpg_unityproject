﻿using RPG.Stats;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Inventories
{
    // Item dropper but drops a random item from the drop library
    public class RandomDropper : ItemDropper
    {
        [SerializeField] float scatterDistance = 1;
        [SerializeField] DropLibrary dropLibrary;

        const int ATTEMPTS = 30;

        public void RandomDrop()
        {
            if (dropLibrary == null) return;
            int level = GetComponent<BaseStats>().GetLevel();
            IEnumerable<DropLibrary.Dropped> drops = dropLibrary.GetRandomDrops(level);
            foreach(var drop in drops)
            {
                DropItem(drop.item, drop.number);
            }
        }


        protected override Vector3 GetDropLocation()
        {
            for(int i = 0; i< ATTEMPTS; i++)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * scatterDistance;
                if (NavMesh.SamplePosition(randomPoint, out NavMeshHit hit, 0.1f, NavMesh.AllAreas))
                {
                    return hit.position;
                }
            }
            return transform.position;
        }
    }
}
