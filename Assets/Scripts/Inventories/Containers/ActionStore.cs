﻿using RPG.Combat;
using RPG.Saving;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{
    // Holds the data for the action items that are in the players action slots
    public class ActionStore : MonoBehaviour,ISaveable
    {
        Dictionary<int, DockedItemSlot> actionItems  = new Dictionary<int, DockedItemSlot>();

        private class DockedItemSlot
        {
            public ActionItem item;
            public int number;
            public DockedItemSlot(ActionItem item, int number)
            {
                this.item = item;
                this.number = number;
            }
        }

        public event Action storeUpdated;

        public static ActionStore GetPlayerActionStore()
        {
            return GameObject.FindGameObjectWithTag("Player").GetComponent<ActionStore>();
        }

        public ActionItem GetAction(int index)
        {
            if(actionItems.ContainsKey(index))
            {
                return actionItems[index].item;
            }
            return null;
        }

        public int GetNumber(int index)
        {
            if (actionItems.ContainsKey(index))
            {
                return actionItems[index].number;
            }
            return 0;
        }

        public void AddAction(InventoryItem item, int index, int number)
        {
            if(actionItems.ContainsKey(index))
            {
                if(object.ReferenceEquals(item,actionItems[index].item))
                {
                    actionItems[index].number += number;
                }
            }
            else
            {
                actionItems[index] = new DockedItemSlot((ActionItem)item, number);
            }
            if(storeUpdated != null)
            {
                storeUpdated();
            }
        }
        // Try using the item
        public bool Use(int index, GameObject user, GameObject target)
        {
            if(actionItems.ContainsKey(index) && actionItems[index].item.Use(user, target))
            {
                if(actionItems[index].item.IsConsumable())
                {
                    RemoveItems(index, 1);
                }
  
                return true;
            }
            return false;
        }


        public void RemoveItems(int index, int number)
        {
            if(actionItems.ContainsKey(index) )
            {
                actionItems[index].number -= number;
                if(actionItems[index].number <= 0)
                {
                    actionItems.Remove(index);
                }
            }
            if (storeUpdated != null)
            {
                storeUpdated();
            }
        }

        public int MaxAcceptable(InventoryItem item, int index)
        {
            var actionItem = item as ActionItem;
            if (!actionItem) return 0;
            if (actionItems.ContainsKey(index) && !object.ReferenceEquals(item, actionItems[index].item)) return 0;
            if (actionItem.IsConsumable()) return int.MaxValue;
            if (actionItems.ContainsKey(index)) return 0;
            return 1;
        }

        [System.Serializable]
        private struct SaveData
        {
            public string itemID;
            public int number;
        }

        public object CaptureState()
        {
            var state = new Dictionary<int, SaveData>();
            foreach(var pair in actionItems)
            {
                if (pair.Value == null) continue; 
                var saveData = new SaveData();
                saveData.itemID = pair.Value.item.GetItemID();
                saveData.number = pair.Value.number;
                state[pair.Key] = saveData;
            }
            return state;
        }

        public void RestoreState(object state)
        {
            var restoredState = (Dictionary<int, SaveData>)state;

            foreach(var pair in restoredState)
            {
                AddAction(InventoryItem.GetFromID(pair.Value.itemID), pair.Key, pair.Value.number);
            }
        }


    }
}