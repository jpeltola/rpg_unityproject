﻿using RPG.Saving;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Inventories
{ 
    // Holds the data for equipped items
    public class Equipment : MonoBehaviour,ISaveable
    {
        Dictionary<EquipmentLocation, EquipableItem> equippedItems = new Dictionary<EquipmentLocation, EquipableItem>();

        public event Action equipmentUpdated;

        public bool AddItemsToSlot(EquipableItem item, int number)
        {
            if (GetComponent<BaseStats>().GetLevel() < item.GetLevelRequired()) return false;
            equippedItems[item.GetEquipmentLocation()] = item;
            equipmentUpdated();
            return true;
        }

        public static Equipment GetPlayerEquipment()
        {
            return GameObject.FindGameObjectWithTag("Player").GetComponent<Equipment>();
        }

        public EquipableItem GetItemInSlot(EquipmentLocation equipmentLocation)
        {
            if(equippedItems.ContainsKey(equipmentLocation))
            {
                return equippedItems[equipmentLocation];
            }
            return null;
        }

        public void RemoveItems(EquipmentLocation equipmentLocation, int number)
        {
            if(equippedItems.ContainsKey(equipmentLocation))
            {
                equippedItems.Remove(equipmentLocation);
            }
            equipmentUpdated();
        }

        public IEnumerable<EquipmentLocation> GetAllPopulatedSlots()
        {
            return equippedItems.Keys;
        }

        public object CaptureState()
        {
            Dictionary<EquipmentLocation, string> saveData = new Dictionary<EquipmentLocation, string>();
            foreach(var item in equippedItems)
            {
                if(item.Value != null)
                {
                    saveData[item.Key] = item.Value.GetItemID();
                }
            }
            return saveData;
        }

        public void RestoreState(object state)
        {
            foreach(var item in (Dictionary<EquipmentLocation, string>)state)
            {
                equippedItems[item.Key] = (EquipableItem)InventoryItem.GetFromID(item.Value);
            }
            
            equipmentUpdated();
        }
    }
}
