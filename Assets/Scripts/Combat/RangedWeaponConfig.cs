﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Attributes;

namespace RPG.Combat
{
    // Configuration for ranged weapon. Inherits WeaponConfig (for melee)
    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Ranged Weapon", order = 1)]
    public class RangedWeaponConfig : WeaponConfig
    {
        [SerializeField] Projectile projectile = null;
        public void LaunchProjectile(Transform rightHand, Transform leftHand, Health target, GameObject instigator, float calculatedDamage)
        {
            Transform handTransform = GetTransform(rightHand, leftHand);
            Projectile projectileInstance = Instantiate(projectile, handTransform.position,Quaternion.identity);
            projectileInstance.SetTarget(target,instigator,calculatedDamage);
            projectileInstance.onHit.AddListener( () => instigator.GetComponent<Fighter>().GetWeapon().onHit?.Invoke(target.gameObject,calculatedDamage));
        }
    }
}
