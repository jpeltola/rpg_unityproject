﻿using RPG.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    // Can make a group of enemies aggressive when certain event is triggered (for example, dialogue).
    public class AggroGroup : MonoBehaviour
    {
        [SerializeField] Fighter[] fighters;
        [SerializeField] bool activateOnStart = false;
        [SerializeField] string campID = "";

        public CampDestroyedEvent onFighterDead = new CampDestroyedEvent();

        public class CampDestroyedEvent : UnityEvent<string>
        {
        }

        private void Start()
        {
            Active(activateOnStart);
            foreach(var fighter in fighters)
            {
                fighter.GetComponent<Health>().onDie.AddListener(OnFighterDead);
            }
        }

        private void OnFighterDead()
        {
            onFighterDead?.Invoke(campID);
        }

        public void Active(bool shouldActivate)
        {
            foreach(Fighter fighter in fighters)
            {
                if (fighter == null) continue;
                CombatTarget target = fighter.GetComponent<CombatTarget>();
                if(target != null)
                {
                    target.enabled = shouldActivate;
                }
                fighter.enabled = shouldActivate;
            }
        }

        public bool IsCleared()
        {
            foreach(var fighter in fighters)
            {
                if (!fighter.GetComponent<Health>().IsDead()) return false;
            }
            return true;
        }
    }
}