﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    // Class for the physical weapon itself (the prefab to be attached to arm). Weapon modifier components are added to this
    public class Weapon : MonoBehaviour
    {
        [SerializeField] public WeaponEvent onHit = new WeaponEvent();
        public WeaponEvent onShoot = new WeaponEvent();
        Fighter owner;

        [System.Serializable]
        public class WeaponEvent: UnityEvent<GameObject,float>
        {
        }

        public void SetOwner(Fighter owner)
        {
            this.owner = owner;
        }
        public Fighter GetOwner()
        {
            return owner;
        }
    }
}
