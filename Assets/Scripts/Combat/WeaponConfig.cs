﻿using UnityEngine;
using System;
using RPG.Inventories;

namespace RPG.Combat
{

    // Weapon configuration for melee weapons
    public enum Hand
    {
        Right, Left, TwoHanded
    }

    [CreateAssetMenu(fileName ="Weapon",menuName = "Weapons/Melee Weapon",order = 0)]
    public class WeaponConfig : StatsEquipableItem
    {

        public enum WeaponType
        {
            Unarmed,
            Dagger,
            Sword,
            Bow,
            Axe,
            Magic
        }

        [SerializeField] AnimatorOverrideController animatorOverride = null;
        [SerializeField] Weapon equippedPrefab = null;

        [SerializeField] float weaponCooldown = 2f;
        [SerializeField] float weaponRange = 2f;
        [SerializeField] bool canRangeBeBoosted = false;
        [SerializeField] Hand hand = Hand.Right;
        [SerializeField] WeaponType weaponType;

        const string weaponName = "Weapon";
        

        public Weapon Spawn(Fighter owner, Transform rightHand,Transform leftHand, Animator animator)
        {
            DestroyOldWeapon(rightHand);
            DestroyOldWeapon(leftHand);
    
            Weapon weapon = null;

            if (equippedPrefab != null)
            {
                Transform handTransform = GetTransform(rightHand, leftHand);
                weapon = Instantiate(equippedPrefab, handTransform);
                weapon.gameObject.name = weaponName;
                weapon.SetOwner(owner);
            }
            
            var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
            if (animatorOverride != null)
            {
                animator.runtimeAnimatorController = animatorOverride;
            }
            else if(overrideController != null)
            {
                animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
            }
            return weapon;
        }

        private void DestroyOldWeapon(Transform handTransform)
        {
            Transform oldWeapon = handTransform.Find(weaponName);
            if (oldWeapon == null) return;
            oldWeapon.name = "DESTROYING";
            Destroy(oldWeapon.gameObject);
        }
        // Get the hand trasform where the weapon prefab will be attached
        protected Transform GetTransform(Transform rightHand, Transform leftHand)
        {
            switch (hand)
            {
                case Hand.Left:
                    return leftHand;
                case Hand.Right:
                    return rightHand;
                case Hand.TwoHanded:
                    return leftHand;
            }
            return null;
        }
        public float GetRange()
        {
            return weaponRange;
        }
        public float GetCooldown()
        {
            return weaponCooldown;
        }
        public bool CanRangeBeBoosted()
        {
            return canRangeBeBoosted;
        }

        public WeaponType GetWeaponType()
        {
            return weaponType;
        }
        public Hand GetHand()
        {
            return hand;
        }

    }
}