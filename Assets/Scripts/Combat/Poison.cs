﻿using RPG.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat
{

    // Poison effect, take damage over time
    [System.Serializable]
    class Poison : Effect
    {
        const float interval = 1;
        float intervalTimer = 1;
        protected override void Update()
        {
            base.Update();
            if(intervalTimer > interval)
            {
                intervalTimer = 0;
                GetComponent<Health>().TakeDamage(GetInstigator(), GetAdditiveAmount() + GetComponent<Health>().GetCurrentHealthPoints()*GetMultiplicativeAmount()/100);
            }
            intervalTimer += Time.deltaTime;
        }
        public override bool CanHaveMultipleInstances()
        {
            return false;
        }
    }
}