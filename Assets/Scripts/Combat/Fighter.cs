﻿using UnityEngine;
using RPG.Movement;
using RPG.Core;
using RPG.Saving;
using RPG.Attributes;
using System;
using RPG.Stats;
using System.Collections.Generic;
using GameDevTV.Utils;
using RPG.Inventories;

namespace RPG.Combat
{
    // Component that is responsible for the combating. Holds the data for current weapon, amount of damage dealt, combat target etc...
    public class Fighter : MonoBehaviour, IAction, ISaveable,IPredicateEvaluator
    {

        [SerializeField] Transform rightHandTransform = null;
        [SerializeField] Transform leftHandTransform = null;
        [SerializeField] WeaponConfig defaultWeapon = null;
        [SerializeField] float maxAttackSpeed = 1000f;
        [SerializeField] float minAttackSpeed = 1f;

        Health target;
        Equipment equipment;

        private float weaponCooldownTimer = Mathf.Infinity;
        bool attacking = false;
        WeaponConfig currentWeaponConfig;
        LazyValue<Weapon> currentWeapon;

        [System.Serializable]
        struct SaveData
        {
            public string weaponName;
        }

        public void EquipWeapon(WeaponConfig weapon)
        {
            currentWeaponConfig = weapon;
            currentWeapon.value = AttachWeapon(weapon);
        }
       
        public Health GetTarget()
        {
            return target;
        }

        public Weapon GetWeapon()
        {
            return currentWeapon.value;
        }

        //Start the attack action
        public void Attack(GameObject combatTarget)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            target = combatTarget.GetComponent<Health>();
        }

        //Check if target can be attacked (has combat target component, is alive, can be moved to and is in range).
        public bool CanAttack(GameObject combatTarget)
        {
            if (combatTarget == null) return false;
            if ((!GetComponent<Mover>().CanMoveTo(combatTarget.transform.position)) &&  !GetIsInRange(combatTarget.transform) ) return false;
            Health TargetToTest = combatTarget.GetComponent<Health>();
            return TargetToTest != null && !TargetToTest.IsDead();
        }
        // Check if character can see the target (is something blocking vision)
        public bool CanSeeTarget(Transform targetTransform)
        {
            if (targetTransform == null) return true;
            Vector3 startPosition = transform.position + Vector3.up * GetComponent<CapsuleCollider>().height / 2;
            Vector3 endPosition = targetTransform.position + Vector3.up * targetTransform.GetComponent<CapsuleCollider>().height / 2;

            Ray ray = new Ray(startPosition, endPosition - startPosition);
            //Get all hits between target and instigator
            RaycastHit[] hits = Physics.RaycastAll(ray, Vector3.Distance(startPosition, endPosition));
            if (hits.Length == 0) return true;

            float[] distances = new float[hits.Length];
            for (int i = 0; i < distances.Length; i++)
            {
                distances[i] = hits[i].distance;
            }
            Array.Sort(distances, hits);
            foreach (RaycastHit hit in hits)
            {
                // if hit object is the target, character can see them
                if (hit.collider.gameObject == targetTransform.gameObject) return true;
                // if hit an object that block visibility, character cannot see them
                if (IsBlockingVisibility(hit)) return false;
            }
            return false;
        }

        public WeaponConfig GetWeaponConfig()
        {
            return currentWeaponConfig;
        }

        public void Cancel()
        {
            target = null;
            attacking = false;
            StopAttack();
        }


        private void Awake()
        {
            currentWeaponConfig = defaultWeapon;
            currentWeapon = new LazyValue<Weapon>(SetupDefaultWeapon);
            equipment = GetComponent<Equipment>();
            if (equipment)
            {
                equipment.equipmentUpdated += UpdateWeapon;
            }

        }

        private Weapon SetupDefaultWeapon()
        {
            return AttachWeapon(defaultWeapon);
        }

        private void Start()
        {
            currentWeapon.ForceInit();
        }


        private void Update()
        {

            UpdateCooldown();

            if (target == null) return;
            if (target.IsDead())
            {
                Cancel();
                return;
            }

            if (!GetIsInRange(target.transform) || !CanSeeTarget(target.transform))
            {
                GetComponent<Mover>().MoveTo(target.transform.position, 1f);
            }
            else
            {
                GetComponent<Mover>().Cancel();
                AttackBehaviour();
            }
        }

        private void UpdateCooldown()
        {
            weaponCooldownTimer += Time.deltaTime;
            GetComponent<Animator>().SetBool("isCooldownGone", weaponCooldownTimer > GetCooldownTime() && !attacking);
        }


        private void StopAttack()
        {
            GetComponent<Animator>().ResetTrigger("attack");
            GetComponent<Animator>().SetTrigger("stopAttack");
        }

        private void AttackBehaviour()
        {
            transform.LookAt(target.transform);
            if (!attacking && weaponCooldownTimer > GetCooldownTime())
            {
                //This will trigger the Hit() event
                TriggerAttack();
            }
        }

        private void UpdateWeapon()
        {
            WeaponConfig weapon = equipment.GetItemInSlot(EquipmentLocation.Weapon) as WeaponConfig;
            if (weapon != null)
            {
                EquipWeapon(weapon);
            }
            else
            {
                EquipWeapon(defaultWeapon);
            }
        }

        //private void UpdateShield()


        private Weapon AttachWeapon(WeaponConfig weapon)
        {
            if (weapon == null) return null;
            Animator animator = GetComponent<Animator>();
            return weapon.Spawn(this, rightHandTransform, leftHandTransform, animator);
        }

        //Cooldown time is modified by attackSpeed modifier. Attackspeed is clamped between min and max attackspeed
        //formula: t = t0/(1+AS/100) 
        private float GetCooldownTime()
        {
            if (currentWeaponConfig == null) return 0;
            float cooldownTime = currentWeaponConfig.GetCooldown()/( Mathf.Clamp( GetComponent<BaseStats>().GetStat(Stat.AttackSpeed),minAttackSpeed, maxAttackSpeed)/100 );
            return cooldownTime;
        }

        private void TriggerAttack()
        {
            attacking = true;
            GetComponent<Animator>().ResetTrigger("stopAttack");
            GetComponent<Animator>().SetTrigger("attack");
        }

        //Animation Event (is called from the animation)
        void Hit()
        {
            if(target == null)return;
            weaponCooldownTimer = 0;
            attacking = false;
            float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);
            if(currentWeapon.value != null)
            {
                currentWeapon.value.onHit?.Invoke(target.gameObject,damage);
            }

            target.TakeDamage(gameObject,damage);
        }

        //AnimationEvent (is called from the animation)
        void Shoot()
        {
            //This is only for ranged weapons
            if (target == null) return;
            weaponCooldownTimer = 0;
            attacking = false;
            
            float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);
            ((RangedWeaponConfig)currentWeaponConfig).LaunchProjectile(rightHandTransform,leftHandTransform,target,gameObject,damage);
            if (currentWeapon.value != null)
            {
                currentWeapon.value.onShoot?.Invoke(target.gameObject,damage);
            }
        }
        // Check if the target is in range
        private bool GetIsInRange(Transform targetTransform)
        {
            float range = currentWeaponConfig.GetRange();
            if(currentWeaponConfig.CanRangeBeBoosted())
            {
                range += GetComponent<BaseStats>().GetStat(Stat.Range);
            }
            return Vector3.Distance(transform.position, targetTransform.transform.position) <  range;
        }

        private bool IsBlockingVisibility(RaycastHit hit)
        {
            
            if (hit.collider.GetComponent<Projectile>() != null) return false;
            if (hit.collider.GetComponent<Health>() != null) return false;
            return true;
        }

        // Saving functions
        public object CaptureState()
        {
            SaveData data = new SaveData();
            data.weaponName = currentWeaponConfig.name;
            return data;
        }
        // Saving functions
        public void RestoreState(object state)
        {
            SaveData restoredData = ((SaveData)state);
            string weaponName = restoredData.weaponName;
            WeaponConfig weapon = UnityEngine.Resources.Load<WeaponConfig>(weaponName);
            EquipWeapon(weapon);
        }
        // Predicate Evaluation function
        public bool? Evaluate(EPredicate predicate, string[] parameters)
        {
            switch(predicate)
            {
                case EPredicate.HasTarget:
                    return GetTarget() != null;
                case EPredicate.HasWeapon:
                    return GetWeapon() != null;
                case EPredicate.WeaponTypeEquipped:
                    if (Enum.GetName(typeof(WeaponConfig.WeaponType), currentWeaponConfig.GetWeaponType()) == parameters[0]) return true;
                    return false;
            }
            return null;
        }
    }
}