﻿using RPG.Combat;
using RPG.Stats;
using RPG.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat
{

    // Base class Component that should be added to a weapon (lifesteal, multishot, etc.. inherited from this)
    [RequireComponent(typeof(Weapon))]
    public abstract class WeaponModifier : MonoBehaviour
    {
        Fighter weaponOwner;
        Weapon weapon;
        [SerializeField] float modifierDuration;
        float timeAffected = 0;


        protected void Initialize()
        {
            weapon = GetComponent<Weapon>();
            weaponOwner = weapon.GetOwner();
        }

        protected virtual void Update()
        {
            if (timeAffected > modifierDuration)
            {
                Destroy(this);
            }
            timeAffected += Time.deltaTime;
        }

        public virtual void Setup(WeaponModifier modifierPrefab)
        {
            weaponOwner = GetComponent<Weapon>().GetOwner();
            modifierDuration = modifierPrefab.GetDuration();
        }

        protected abstract void Trigger(GameObject target,float damage);

        public float GetDuration()
        {
            return modifierDuration;
        }

        protected Weapon GetWeapon()
        {
            return weapon;
        }
        protected Fighter GetOwner()
        {
            return weaponOwner;
        }

    }
}