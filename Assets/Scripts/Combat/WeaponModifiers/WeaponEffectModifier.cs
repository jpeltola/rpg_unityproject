﻿using RPG.Attributes;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat
{
    // Weapon modifier, applies effect on target or weapon owner, when hit is triggered (for example, poison)
    public class WeaponEffectModifier : WeaponModifier
    {
        [SerializeField] EffectData[] effectsOnTarget;
        [SerializeField] EffectData[] effectsOnOwner;

        private void Start()
        {
            Initialize();
            GetComponent<Weapon>().onHit.AddListener(Trigger);
        }
        public override void Setup(WeaponModifier modifierPrefab)
        {
            base.Setup(modifierPrefab);
            WeaponEffectModifier effectModifier = (WeaponEffectModifier)modifierPrefab;
            effectsOnOwner = effectModifier.GetEffectsOnOwner();
            effectsOnTarget = effectModifier.GetEffectsOnTarget();

        }

        private EffectData[] GetEffectsOnTarget()
        {
            return effectsOnTarget;
        }

        private EffectData[] GetEffectsOnOwner()
        {
            return effectsOnOwner;
        }

        protected override void Trigger(GameObject target, float damage)
        {
            foreach (var effectData in effectsOnOwner)
            {
                Effect effect = AddEffect(effectData, GetOwner().gameObject);
                if (effect == null) continue;
                effect.Setup(effectData, GetOwner().gameObject);
            }

            foreach (var effectData in effectsOnTarget)
            {
                Effect effect = AddEffect(effectData, target);
                if (effect == null) continue;
                effect.Setup(effectData, GetOwner().gameObject);
            }
        }
        Effect AddEffect(EffectData effect, GameObject target)
        {
            Type type = Effect.GetType(effect.type);

            Component[] appliedEffects = target.GetComponents(type);
            foreach (var component in appliedEffects)
            {
                Effect appliedEffect = component as Effect;
                if ( (appliedEffect.GetInstigator() == GetOwner().gameObject && !appliedEffect.DoesStack()) || !appliedEffect.CanHaveMultipleInstances())
                {
                    Destroy(appliedEffect);
                    break;
                }
            }

            if (type == null) return null;
            return target.AddComponent(type) as Effect;
        }


    }
}