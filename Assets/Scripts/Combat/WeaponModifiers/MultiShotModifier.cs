﻿using RPG.Attributes;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat
{
    // When applied to a ranged weapon prefab, shoot multiple projectiles to nearby enemies
    class MultiShotModifier : WeaponModifier
    {
        RangedWeaponConfig weaponConfig;
        [SerializeField] float damageFraction;
        [SerializeField] int maxAdditionalTargets;

        private void Start()
        {
            Initialize();
            weaponConfig = GetOwner().GetWeaponConfig() as RangedWeaponConfig;
            GetWeapon().onShoot.AddListener(Trigger);
        }

        protected override void Trigger(GameObject target,float damage)
        {
            SortedHits(out RaycastHit[] hits);
            int targets = 0;
            foreach (var hit in hits)
            {
                
                if (targets >= maxAdditionalTargets) return;
                CombatTarget combatTarget = hit.collider.GetComponent<CombatTarget>();
                if (combatTarget != null && GetOwner().GetComponent<CombatTarget>() != combatTarget && combatTarget != target.GetComponent<CombatTarget>())
                {
                    ++targets;
                    weaponConfig.LaunchProjectile(GetWeapon().transform, GetWeapon().transform, combatTarget.GetComponent<Health>(), GetOwner().gameObject, damage * damageFraction/100);
                }
            }
        }

        private void SortedHits(out RaycastHit[] hits)
        {
            hits = Physics.SphereCastAll(transform.position, weaponConfig.GetRange(), Vector3.up, 0);
            float[] distances = new float[hits.Length];
            for(int i = 0; i < distances.Length;i++)
            {
                distances[i] = hits[i].distance;
            }
            Array.Sort(distances, hits);
        }

        protected override void Update()
        {
            base.Update();
        }
    }
}