﻿using RPG.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat
{
    // When applied to weapon prefab, grants lifesteal modifier (returns health to wielder)
    public class LifeSteal : WeaponModifier
    {
        [SerializeField] float amount;
        [SerializeField] float damagePercentage;

        private void Start()
        {
            Initialize();
            GetComponent<Weapon>().onHit.AddListener(Trigger);
        }
        protected override void Trigger(GameObject target, float damage)
        {
            GetOwner().GetComponent<Health>().Heal(amount + damage * damagePercentage / 100);

        }
    }
}
