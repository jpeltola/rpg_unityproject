﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Spawns the persistent objects to the world when loaded
public class PersistentObjectSpawner : MonoBehaviour
{
    [SerializeField] GameObject persistentObjectPrefab;

    static bool hasSpawned = false;
    private void Awake()
    {
        if (hasSpawned) return;

        hasSpawned = true;

        SpawnPersistentObjects();
    }

    private void SpawnPersistentObjects()
    {
        GameObject persistentObject = Instantiate(persistentObjectPrefab);
        DontDestroyOnLoad(persistentObject);
    }
}
