﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    // interface for the predicate evaluators: for example to predicate HasItem, Inventory is a IPredicateEvaluator
    public interface IPredicateEvaluator 
    {
        bool? Evaluate(EPredicate predicate, string[] parameters); 
    }
}
