﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    //Class for calculating the stat value for each level
    [System.Serializable]
    class ProgressionStatFormula
    {
        [SerializeField] float startingValue = 0;
        [Range(0, 1)]
        [SerializeField] float percentageAdded = 0;
        [SerializeField] float absoluteAdded = 0;

        public float Calculate(int level)
        {
            if (level <= 1) return startingValue;
            float c = Calculate(level - 1);
            return c + (c * percentageAdded) + absoluteAdded;
        }
    }
}