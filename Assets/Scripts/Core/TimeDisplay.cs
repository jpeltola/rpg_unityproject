﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.InGameTime
{
    // UI element for showing the global time
    public class TimeDisplay : MonoBehaviour
    {
        IngameTimeGlobal timeObject;

        private void Start()
        {
            timeObject = FindObjectOfType<IngameTimeGlobal>();
        }
        void Update()
        {
            if(timeObject != null)
            {
                float remainder = timeObject.GetCurrentTime();
                float hours = Mathf.Floor(remainder / 3600);
                remainder = remainder % 3600;
                float minutes = Mathf.Floor(remainder / 60);
                float seconds = Mathf.Floor(remainder % 60);
                GetComponent<Text>().text = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
            }
        }
    }
}
