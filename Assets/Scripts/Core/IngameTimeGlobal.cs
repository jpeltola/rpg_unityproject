﻿using GameDevTV.Utils;
using RPG.Saving;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.InGameTime
{
    // Holds the global time played
    public class IngameTimeGlobal : MonoBehaviour, ISaveable
    {
        float time = 0;

        // Start is called before the first frame update
        private void Update()
        {
            time += Time.deltaTime;
        }

        public float GetCurrentTime()
        {
            return time;
        }
        //Returns the time that has been since last save
        public float GetDeltaTime()
        {
            return time - FindObjectOfType<IngameTime>().GetCurrentTime();
        }

        public object CaptureState()
        {
            return time;
        }

        public void RestoreState(object state)
        {
            time = (float)state;
        }

    }
}
