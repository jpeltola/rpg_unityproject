﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    // Is responsible for characters actions (movement, fighting, etc..) cancel movement when fighting and cancel fightin when moving action is started
    public class ActionScheduler : MonoBehaviour
    {
        IAction currentAction;
        public void StartAction(IAction action)
        {
            if (currentAction == action) return;
            if(currentAction != null)
            {
                currentAction.Cancel();         
            }
            currentAction = action;
        }
        public void CancelCurrentAction()
        {
            StartAction(null);
        }
    }

}