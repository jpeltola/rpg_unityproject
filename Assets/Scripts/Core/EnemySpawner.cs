﻿using RPG.Attributes;
using RPG.InGameTime;
using RPG.Saving;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    // Object that respawns the enemy after a certain amount of time they have been dead
    public class EnemySpawner : MonoBehaviour, ISaveable
    {
        [SerializeField] float respawnTime = 30f;
        [Range(0, 1)]
        [SerializeField] float despawnTimeFraction = 0.5f;
        [SerializeField] GameObject spawnObjectPrefab;


        GameObject spawnInstance;

        float timeSinceDeath = Mathf.Infinity;

        bool loaded = false;

        [Serializable]
        struct SaveData
        {
            public float timeSinceDeath;
            public float timeStamp;
        }

        private void Awake()
        {
            spawnObjectPrefab.SetActive(false);
            Respawn();
        }

        void Update()
        {
            if(loaded)
            {
                timeSinceDeath = timeSinceDeath + FindObjectOfType<IngameTimeGlobal>().GetDeltaTime();
                loaded = false;
            }
            if (IsDead() && timeSinceDeath > respawnTime * despawnTimeFraction)
            {
                Despawn();
            }
            if (IsDead() && timeSinceDeath > respawnTime)
            {
                Respawn();
            }
            UpdateTimer();
        }

        private void UpdateTimer()
        {
            if (!IsDead()) return;
            timeSinceDeath += Time.deltaTime;
        }

        private void SetRespawnTimer()
        {
            timeSinceDeath = 0;
        }

        private void Respawn()
        {
            
            if (spawnInstance != null)
            {
                Destroy(spawnInstance.gameObject);
            }
                
            spawnInstance = Instantiate(spawnObjectPrefab, transform);
            spawnInstance.SetActive(true);
            spawnInstance.GetComponent<Health>().onDie.AddListener(SetRespawnTimer);
        }

        private void Despawn()
        {
            if (spawnInstance == null) return;
            spawnInstance.GetComponent<Health>().onDie.RemoveListener(SetRespawnTimer);
            spawnInstance.transform.localScale = new Vector3(0, 0, 0);
        }
        private bool IsDead()
        {
            return spawnInstance == null || spawnInstance.GetComponent<Health>().IsDead();
        }

        public object CaptureState()
        {
            SaveData saveData = new SaveData();
            float time = FindObjectOfType<IngameTime>().GetCurrentTime();
            saveData.timeSinceDeath = timeSinceDeath;
            saveData.timeStamp = time;
            return saveData;
        }

        public void RestoreState(object state)
        {
            loaded = true;
            SaveData saveData = (SaveData)state;
            timeSinceDeath = saveData.timeSinceDeath;
            if(spawnInstance != null)
            {
                spawnInstance.transform.localScale = new Vector3(1, 1, 1);
            }
            
        }
    }
}
