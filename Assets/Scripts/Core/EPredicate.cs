﻿

namespace RPG.Core
{
    // List of predicate types
    public enum EPredicate
    {
        HasQuest,
        HasItem,
        QuestCompleted,
        ActionOnCooldown,
        IsDead,
        HasTarget,
        HasWeapon,
        IsCampCleared,
        WeaponTypeEquipped,
    }
}