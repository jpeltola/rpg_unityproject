﻿namespace RPG.Core
{
    // Interface for different actions ( movement, fighting, pickup, dialogue). ActionScheluder manages these
    public interface IAction
    {
        void Cancel();
    }
}