﻿using GameDevTV.Utils;
using RPG.Saving;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.InGameTime
{
    // Holds the ingame time that the game has been played (this is level specific) this is for the case that the player has been away from a level and the game can calculate some cooldowns happening in the level
    // When away
    public class IngameTime : MonoBehaviour, ISaveable
    {
        float time = 0;

        // Start is called before the first frame update
        private void Update()
        {
            time += Time.deltaTime;
        }

        internal float GetCurrentTime()
        {
            return time;
        }

        public object CaptureState()
        {
            return FindObjectOfType<IngameTimeGlobal>().GetCurrentTime();
        }

        public void RestoreState(object state)
        {
            time = (float)state;
        }
    }
}
