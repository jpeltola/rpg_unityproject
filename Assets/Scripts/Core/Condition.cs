﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{

    // Condition is a list of disjunctions (A AND B AND C...) -> if any of A, B, C is false -> condition is false, else true
    // A B and C are list of conjunctions (A = a OR b OR c...) -> if any of a, b, c is true -> disjunction is true, else false
    [System.Serializable]
    public class Condition
    {
        [SerializeField]
        Disjunction[] and;

        // Check if the condition is true or false. 
        public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
        {
            // Go through all the AND predicates (disjunctions) for this condition and check their values
            foreach(var pred in and)
            {
                // If any of the predicates in disjunction is false, the whole condition is false
                if(!pred.Check(evaluators))
                {
                    return false;
                }
            }
            // If none of the predicates are false, the condition is true
            return true;
        }

        
        [System.Serializable]
        class Disjunction
        {
            [SerializeField]
            Predicate[] or; // List of conjunctions
            public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
            {
                // Go through all th OR predicates (conjunctions) in a certain disjunction
                foreach(var pred in or)
                {
                    // if any OR predicate is true, this predicate is true
                    if(pred.Check(evaluators))
                    {
                        return true;
                    }
                }
                // if none of the OR predicates are true, this predicate is false
                return false;
            }
        }

        // Single predicate inside an OR list 
        [System.Serializable]
        class Predicate
        {
            [SerializeField] EPredicate predicate; // predicate type (for example HasQuest, HasItem...)
            [SerializeField] string[] parameters; // parameters to help with the evaluation (for example QuestID, ItemID...)
            [SerializeField] bool negate = false; // negate the predicate

            // Go through all the evaluators and Evaluate the perdicate with them until one of them can answer the result
            public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
            {
                foreach (var evaluator in evaluators)
                {
                    bool? result = evaluator.Evaluate(predicate, parameters);
                    if (result == null)
                    {
                        continue;
                    }
                    if (result == negate) return false;
                }
                return true;
            }
        }
    }

}