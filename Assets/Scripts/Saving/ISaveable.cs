﻿using UnityEngine;
using System.Collections;

namespace RPG.Saving
{
    // Interface for a saveable component
    public interface ISaveable
     {
        object CaptureState();
        void RestoreState(object state);


     }
}