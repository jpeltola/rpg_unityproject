﻿using RPG.Quests;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RPG.UI.Quests
{
    public class QuestItemUI : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI title;
        [SerializeField] TextMeshProUGUI progress;

        QuestStatus status;
        public void Setup(QuestStatus status)
        {
            this.status = status;
            title.text = status.GetQuest().GetTitle();
            progress.text = status.GetCompletedObjectiveCount() +"/" + status.GetQuest().GetObjectiveCount();
            if(status.IsComplete())
            {
                progress.color = Color.gray;
                title.color = Color.gray;
            }
        }

        public QuestStatus GetQuestStatus()
        {
            return status;
        }
    }
}