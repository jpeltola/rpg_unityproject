﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Quests;
using TMPro;

namespace RPG.UI.Quests
{
    public class QuestTooltipUI : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI title;
        [SerializeField] Transform objectiveContainer;
        [SerializeField] GameObject objectiveCompletedPrefab;
        [SerializeField] GameObject objectiveIncompletePrefab;
        [SerializeField] TextMeshProUGUI rewardText;

        public void Setup(QuestStatus status)
        {
            if (status == null || objectiveCompletedPrefab == null || objectiveIncompletePrefab == null) return;
            Quest quest = status.GetQuest();
            title.text = quest.GetTitle();
            foreach(Transform child in objectiveContainer)
            {
                Destroy(child.gameObject);
            }
            foreach(var objective in quest.GetObjectives())
            {
                GameObject prefab = objectiveIncompletePrefab;

                if(status.IsCompleted(objective.reference))
                {
                    prefab = objectiveCompletedPrefab;
                }
                GameObject objectiveInstance = Instantiate(prefab, objectiveContainer);
                objectiveInstance.GetComponentInChildren<TextMeshProUGUI>().text = objective.description;
            }
            rewardText.text = GetRewardText(quest);

        }

        private string GetRewardText(Quest quest)
        {
            string rewardtext = "";
            foreach (var reward in quest.GetRewards())
            {
                if(rewardtext != "")
                {
                    rewardtext += ", ";
                }
                if (reward.number > 1)
                {
                    rewardtext += reward.number + " ";
                }
                rewardtext += reward.item.GetDisplayName();
            }
            if(rewardtext == "")
            {
                rewardtext = "No Reward";
            }
            return rewardtext+".";
        }
    }
}


