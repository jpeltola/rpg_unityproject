﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RPG.Inventories;
using System;

namespace RPG.UI.Inventories
{
    /// <summary>
    /// To be put on the icon representing an inventory item. Allows the slot to
    /// update the icon and number.
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class InventoryItemIcon : MonoBehaviour
    {
        [SerializeField] GameObject textContainer = null;
        [SerializeField] Text itemNumber = null;

        InventoryItem item;
        // PUBLIC

        public void SetItem(InventoryItem item, int number)
        {
            var iconImage = GetComponent<Image>();
            this.item = item;
            if (item == null)
            {
                iconImage.enabled = false;
            }
            else
            {
                iconImage.enabled = true;
                iconImage.sprite = item.GetIcon();
            }
      
            if(number <= 1 )
            {
                textContainer.SetActive(false);
            }
            else
            {
                textContainer.SetActive(true);
                if (number > 99) itemNumber.text = "+99";
                else itemNumber.text = number.ToString();
            }
        }
    }
}