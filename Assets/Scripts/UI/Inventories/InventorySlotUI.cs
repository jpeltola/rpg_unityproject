﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RPG.Inventories;
using RPG.Utils.UI.Dragging;
using System;

namespace RPG.UI.Inventories
{
    public class InventorySlotUI : MonoBehaviour, IDragContainer<InventoryItem>, IItemHolder, IPointerClickHandler
    {
        // CONFIG DATA
        [SerializeField] InventoryItemIcon icon = null;

        // STATE
        int index;
        InventoryItem item;
        Inventory inventory;

        // PUBLIC

        public void Setup(Inventory inventory, int index)
        {
            this.inventory = inventory;
            this.index = index;
            this.item = inventory.GetItemInSlot(index);
            icon.SetItem(inventory.GetItemInSlot(index), inventory.GetNumberInSlot(index));
        }

        public int MaxAcceptable(InventoryItem item)
        {
            if (inventory.HasSpaceFor(item))
            {
                return int.MaxValue;
            }
            return 0;
        }

        public void AddItems(InventoryItem item, int number)
        {
            inventory.AddItemToSlot(index, item, number);
        }

        public InventoryItem GetItem()
        {
            return inventory.GetItemInSlot(index);
        }

        public int GetNumber()
        {
            return inventory.GetNumberInSlot(index);
        }

        public void RemoveItems(int number)
        {
            inventory.RemoveFromSlot(index, number);
        }

        float time = -1;
        public void OnPointerClick(PointerEventData eventData)
        {
            
            if (eventData.clickTime - time < 0.5f )
            {
                if (item != null)
                {  
                    SwapEquipableItemContainer();
                }
            }
            time = eventData.clickTime;
        }

        private void SwapEquipableItemContainer()
        {
            Equipment playerEquipment = Equipment.GetPlayerEquipment();

            EquipableItem equipableItem = item as EquipableItem;
            EquipableItem itemEquipped = playerEquipment.GetItemInSlot(equipableItem.GetEquipmentLocation());
            if (playerEquipment.AddItemsToSlot(equipableItem, 1))
            {
                inventory.RemoveFromSlot(index, 1);
                if(itemEquipped != null)
                {
                    inventory.AddToFirstEmptySlot(itemEquipped, 1);
                }
            }
        }
    }
}