﻿using RPG.Core;
using RPG.Inventories;
using RPG.Utils.UI.Dragging;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.UI.Inventories
{
    public class ActionSlotUI : MonoBehaviour, IDragContainer<InventoryItem>, IItemHolder
    {
        [SerializeField] InventoryItemIcon icon;
        [SerializeField] GameObject cooldownIcon;
        [SerializeField] int index;
        ActionStore store;
        ActionItem item;
        int number;

        private void Awake()
        {
            store = ActionStore.GetPlayerActionStore();
            store.storeUpdated += Setup;
            store.gameObject.GetComponent<CooldownManager>().onCooldownAdded += HandleCooldownUpdated;
            Setup();
        }

        void HandleCooldownUpdated()
        {
            if (item != null)
            {
                StartCoroutine(CooldownUIRoutine());
            }

        }

        private IEnumerator CooldownUIRoutine()
        {
            float cooldown = item.GetCooldown();
            while(item != null)
            {
                yield return SetCooldownScale(item.CooldownRemaining(store.gameObject) / cooldown);
            }
            yield return SetCooldownScale(0);
        }

        private IEnumerator SetCooldownScale(float scale)
        {
            yield return cooldownIcon.transform.localScale = new Vector3(1, scale, 1);
        }


        public void Setup()
        {
            item = store.GetAction(index);
            number = store.GetNumber(index);
            icon.SetItem(item, number);
            HandleCooldownUpdated();
        }

        public void AddItems(InventoryItem item, int number)
        {
            store.AddAction(item,index, number);
        }

        public InventoryItem GetItem()
        {
            return item;
        }

        public int GetNumber()
        {
            return number;
        }

        public int MaxAcceptable(InventoryItem item)
        {
            return store.MaxAcceptable(item,index);
        }

        public void RemoveItems(int number)
        {
            store.RemoveItems(index, number);
        }
    }
}