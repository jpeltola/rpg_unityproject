﻿using RPG.Inventories;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.UI.Inventories
{
    public class EquipmentUI : MonoBehaviour
    {

        Equipment playerEquipment;

        private void Awake()
        {
            playerEquipment = Equipment.GetPlayerEquipment();
            playerEquipment.equipmentUpdated += Redraw;
        }
        // Start is called before the first frame update
        void Start()
        {
            Redraw();
        }

        private void Redraw()
        {
            foreach(var equipmentSlot in GetComponentsInChildren<EquipmentSlotUI>())
            {
                equipmentSlot.Setup(playerEquipment);
            }
        }
    }
}
