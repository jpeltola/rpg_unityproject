﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Utils.UI;
using RPG.Inventories;
using RPG.Utils.UI.Dragging;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using RPG.Stats;

namespace RPG.UI.Inventories
{
    public class EquipmentSlotUI : MonoBehaviour, IDragContainer<InventoryItem>,IItemHolder,IPointerClickHandler
    {
        [SerializeField] InventoryItemIcon icon;
        [SerializeField] EquipmentLocation equipmentLocation;

        EquipableItem item;
        Equipment equipment;

        public void Setup(Equipment equipment)
        {
            this.equipment = equipment;
            item = equipment.GetItemInSlot(equipmentLocation);
            icon.SetItem(item, 1);
        }

        public void AddItems(InventoryItem item, int number)
        {
            equipment.AddItemsToSlot((EquipableItem)item,number);
        }
        public InventoryItem GetItem()
        {
            return item;
        }

        public int GetNumber()
        {
            if(item == null)
            {
                return 0;
            }
            return 1;
        }

        public int MaxAcceptable(InventoryItem item)
        {
            if(! (item is EquipableItem) ) return 0;
            if (((EquipableItem)item).GetLevelRequired() > equipment.GetComponent<BaseStats>().GetLevel()) return 0;
            var equipableItem = item as EquipableItem;
            if(equipableItem.GetEquipmentLocation() != equipmentLocation) return 0;

            if(this.item != null) return 0;

            return 1;
        }

        public void RemoveItems(int number)
        {
            equipment.RemoveItems(equipmentLocation, number);
        }



        float time = -1;
        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.clickTime - time < 0.5f && item != null) 
            {
                bool success = Inventory.GetPlayerInventory().AddToFirstEmptySlot(item, 1);
                if(success) RemoveItems(1);
            }
            time = eventData.clickTime;
        }
    }
}
