﻿using RPG.Inventories;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemTooltip : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI title = null;
    [SerializeField] TextMeshProUGUI body = null;

    public void Setup(InventoryItem item)
    {
        title.text = item.GetDisplayName();
        body.text = item.GetDescription();
    }
}
