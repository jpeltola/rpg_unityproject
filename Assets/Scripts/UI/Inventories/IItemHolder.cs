﻿using RPG.Inventories;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.UI.Inventories
{
    public interface IItemHolder
    {
        InventoryItem GetItem();
    }

}