﻿using GameDevTV.Core.UI.Tooltips;
using RPG.Inventories;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.UI.Inventories
{
    public class ItemTooltipSpawner : TooltipSpawner
    {
        public override bool CanCreateTooltip()
        {
            return GetComponent<IItemHolder>().GetItem() != null;
        }

        public override void UpdateTooltip(GameObject tooltip)
        {
            InventoryItem item = GetComponent<IItemHolder>().GetItem();
            tooltip.GetComponent<ItemTooltip>().Setup(item);
        }
    }
}
