﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.UI.DamageText
{
    public class DamageText : MonoBehaviour
    {
        [SerializeField] Text damageText = null;
        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void Set(float damage,Color color)
        {
            damageText.text = String.Format("{0:0}",damage);
            damageText.color = color;
        }
        
    }
}
