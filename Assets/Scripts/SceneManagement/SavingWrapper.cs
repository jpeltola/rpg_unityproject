﻿using UnityEngine;
using System.Collections;
using RPG.Saving;

namespace RPG.SceneManagement
{
    // Interface between the game and saving system
    public class SavingWrapper : MonoBehaviour
    {
        [SerializeField] float fadeTime = 0.2f;
        [SerializeField] KeyCode saveKey = KeyCode.O;
        [SerializeField] KeyCode loadKey = KeyCode.L;
        [SerializeField] KeyCode deleteKey = KeyCode.Delete;
        const string defaultSaveFile = "save";


        private void Awake()
        {
            //StartCoroutine(LoadLastScene());
            StartCoroutine(FirstLoad());
        }

        private IEnumerator FirstLoad()
        {
            yield return LoadLastScene();
            Fader fader = FindObjectOfType<Fader>();
            fader.FadeOutImmediate();
            yield return fader.Fade(0, fadeTime);
        }
        private IEnumerator LoadLastScene()
        {
            yield return GetComponent<SavingSystem>().LoadLastScene(defaultSaveFile);
            
        }

        private void Update()
        {
            if(Input.GetKeyDown(saveKey))
            {
                Save();
            }
            if (Input.GetKeyDown(loadKey))
            {
                //Load();
                StartCoroutine(LoadLastScene());
            }
            if(Input.GetKeyDown(deleteKey))
            {
                Delete();
            }
        }

        public void Save()
        {
            GetComponent<SavingSystem>().Save(defaultSaveFile);
        }
        public void Load()
        {
            GetComponent<SavingSystem>().Load(defaultSaveFile);
        }
        public void Delete()
        {
            GetComponent<SavingSystem>().Delete(defaultSaveFile);
        }
    }
}