﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Control;

namespace RPG.Dialogue
{
    // Component for the AI that holds dialogue, responsible for mouse raycasting and the dialogue the AI has.
    public class AIConversant : MonoBehaviour, IRaycastable
    {
        [SerializeField] Dialogue dialogue;
        [SerializeField] float conversationDistance = 4f;
        [SerializeField] string conversantName;
        public CursorType GetCursorType()
        {
            return CursorType.Dialogue;
        }

        public bool HandleRayCast(PlayerController callingController)
        {
            if (dialogue == null) return false;
            
            if(Input.GetMouseButtonDown(0))
            {
                callingController.GetComponent<PlayerConversant>().StartDialogueBehavior(this, dialogue);
            }
            return true; 
        }
        public string GetConversantName()
        {
            return conversantName;
        }

        public float GetConversationDistance()
        {
            return conversationDistance;
        }
    }
}
