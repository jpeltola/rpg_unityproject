﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace RPG.Dialogue.Editor
{
    public class DialogueEditor : EditorWindow
    {
        Dialogue selectedDialogue = null;
        [NonSerialized]
        GUIStyle nodeStyle;
        [NonSerialized]
        GUIStyle playerNodeStyle;
        [NonSerialized]
        DialogueNode draggingNode = null;
        [NonSerialized]
        Vector2 draggingNodeOffset;
        [NonSerialized]
        DialogueNode creatingNode = null;
        [NonSerialized]
        DialogueNode deletingNode = null;
        [NonSerialized]
        DialogueNode linkingParent = null;
        Vector2 scrollPosition;
        [NonSerialized]
        bool draggingCanvas = false;
        [NonSerialized]
        Vector2 draggingCanvasOffset;

        const float canvasSize = 4000;
        const float backGroundSize = 50;


        [MenuItem("Window/Dialogue Editor")]
        public static void ShowEditorWindow()
        {
            EditorWindow window = GetWindow(typeof(DialogueEditor), false, "Dialogue Editor");
            EditorStyles.textField.wordWrap = true;

        }
        [OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            Dialogue dialogue = EditorUtility.InstanceIDToObject(instanceID) as Dialogue;
            if (dialogue == null) return false;
            
            ShowEditorWindow();
            return true;
        }
        private void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChanged;

            nodeStyle = new GUIStyle();
            nodeStyle.normal.background = EditorGUIUtility.Load("node0") as Texture2D;
            nodeStyle.padding = new RectOffset(20, 20, 20, 20);
            nodeStyle.border = new RectOffset(12, 12, 12, 12);

            playerNodeStyle = new GUIStyle();
            playerNodeStyle.normal.background = EditorGUIUtility.Load("node1") as Texture2D;
            playerNodeStyle.padding = new RectOffset(20, 20, 20, 20);
            playerNodeStyle.border = new RectOffset(12, 12, 12, 12);

        }
        private void OnSelectionChanged()
        {
            Dialogue newDialogue = Selection.activeObject as Dialogue;
            if(newDialogue != null)
            {
                selectedDialogue = newDialogue;
                Repaint();
            }
        }

        private void OnGUI()
        {
            if(selectedDialogue == null)
            {
                EditorGUILayout.LabelField("No Dialogue Selected");
            }
            else
            {
                ProcessEvents();

                scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

                Rect canvas = GUILayoutUtility.GetRect(canvasSize, canvasSize);
                Texture2D backgroundTex = Resources.Load("background") as Texture2D;

                Rect texCoords = new Rect(0, 0, canvasSize/backGroundSize, canvasSize / backGroundSize);
                GUI.DrawTextureWithTexCoords(canvas, backgroundTex,texCoords);

                foreach (var node in selectedDialogue.GetAllNodes())
                {
                    DrawConnections(node);
                }
                foreach (var node in selectedDialogue.GetAllNodes())
                {
                    DrawNode(node);
                }
                EditorGUILayout.EndScrollView();

                if (creatingNode !=null)
                {
                    selectedDialogue.CreateNode(creatingNode);
                    
                    creatingNode = null;
                }
                if(deletingNode != null)
                {
                    selectedDialogue.DeleteNode(deletingNode);
                    creatingNode = null;
                }
            }
        }

        private void ProcessEvents()
        {
            if (Event.current.type == EventType.MouseDown && draggingNode == null)
            {   
                draggingNode = GetNodeAtPoint(Event.current.mousePosition + scrollPosition);
                if(draggingNode != null && Event.current.button == 0)
                {
                    draggingNodeOffset = draggingNode.GetRect().position - Event.current.mousePosition;
                    Selection.activeObject = draggingNode;
                }
                else if (Event.current.button == 1)
                {
                    draggingCanvas = true;
                    draggingCanvasOffset = scrollPosition + Event.current.mousePosition;
                }
                else
                {
                    Selection.activeObject = selectedDialogue;
                }
            }
            else if (Event.current.type == EventType.MouseDrag && Event.current.button == 0 && draggingNode != null)
            {
                Undo.RecordObject(selectedDialogue, "Move Dialogue Node");
                draggingNode.SetPosition(Event.current.mousePosition + draggingNodeOffset);
                GUI.changed = true;
            }

            else if (Event.current.type == EventType.MouseDrag && draggingCanvas == true)
            {
                scrollPosition = (draggingCanvasOffset - Event.current.mousePosition);
                
                GUI.changed = true;
            }
            else if(Event.current.type == EventType.MouseUp && draggingNode != null)
            {
                draggingNode = null;
            }
            else if(Event.current.type == EventType.MouseUp && draggingCanvas)
            {
                draggingCanvas = false;
            }

        }

        private void DrawNode(DialogueNode node)
        {
            GUIStyle style = nodeStyle;
            if(node.IsPlayerSpeaking())
            {
                style = playerNodeStyle;
            }

            GUILayout.BeginArea(new Rect(node.GetRect()), style);

            node.SetText(EditorGUILayout.TextField(node.GetText(),GUILayout.Height(100)));
            
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("-"))
            {
                deletingNode = node;
            }

            DrawLinkButtons(node);

            if (GUILayout.Button("+"))
            {
                creatingNode = node;
            }

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        private void DrawLinkButtons(DialogueNode node)
        {
            if (linkingParent == null)
            {
                if (GUILayout.Button("Link"))
                {
                    linkingParent = node;
                }
            }
            else if (linkingParent == node)
            {
                if (GUILayout.Button("Cancel"))
                {
                    linkingParent = null;
                }
            }
            else if (!linkingParent.GetChildren().Contains(node.name))
            {
                if (GUILayout.Button("Child"))
                {
                    linkingParent.AddChild(node.name);
                    linkingParent = null;
                }
            }
            else
            {
                if (GUILayout.Button("Unlink"))
                {
                    linkingParent.RemoveChild(node.name);
                    linkingParent = null;
                }
            }      
        }

        private void DrawConnections(DialogueNode node)
        {
            foreach(var child in selectedDialogue.GetAllChildren(node))
            {             
                Vector3 startPosition = new Vector2(node.GetRect().xMax, node.GetRect().center.y);
                Vector3 endPosition = new Vector2(child.GetRect().xMin, child.GetRect().center.y);
                Vector3 offset = new Vector2(Mathf.Abs(endPosition.x - startPosition.x)/2, 0);
                Handles.DrawBezier(startPosition, endPosition, startPosition + offset, endPosition - offset, Color.white,null,4f);
            }
        }

        private DialogueNode GetNodeAtPoint(Vector2 point)
        {
            DialogueNode foundNode = null;
            foreach(var node in selectedDialogue.GetAllNodes())
            {
                if (!node.GetRect().Contains(point)) continue;
                foundNode = node;
            }
            return foundNode;
        }
    }
}