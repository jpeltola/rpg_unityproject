﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Dialogue
{
    // trigger that can be added to dialogue node, invokes an event, (when node is entered or exited)
    public class DialogueTrigger : MonoBehaviour
    {
        [SerializeField] TriggerAction[] triggers;
        

        [System.Serializable]
        class TriggerAction
        {
            public string action;
            public UnityEvent onTrigger;
        }

        public void Trigger(string actionToTrigger)
        {
            foreach(var trigger in triggers)
            {
                if (actionToTrigger == trigger.action)
                {
                    trigger.onTrigger.Invoke();
                }
            }

        }
    }

}