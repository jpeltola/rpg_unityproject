﻿using RPG.Core;
using RPG.Movement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPG.Dialogue
{
    // Component for the player to manage the dialogue
    public class PlayerConversant : MonoBehaviour, IAction
    {
        [SerializeField] string playerName;
        Dialogue currentDialogue;
        DialogueNode currentNode = null;
        AIConversant currentConversant = null;
        bool isChoosing = false;
        bool isInDialogue = false;
        public event Action onConversationUpdated;

        private void Update()
        {
            if (currentDialogue == null) return;

            if(Vector3.Distance(currentConversant.transform.position,transform.position) > currentConversant.GetConversationDistance())
            {
                GetComponent<Mover>().MoveTo(currentConversant.transform.position,1f);
            }
            else if(!isInDialogue)
            {
                isInDialogue = true;
                GetComponent<Mover>().Cancel();
                StartDialogue();
            }
        }

        private void StartDialogue()
        {
            TriggerEnterActions();
            onConversationUpdated();
        }

        public void StartDialogueBehavior(AIConversant newConversant ,Dialogue newDialogue)
        {
            if (newDialogue == null) return;
            currentConversant = newConversant;
            currentDialogue = newDialogue;
            currentNode = currentDialogue.GetRootNode();
            GetComponent<ActionScheduler>().StartAction(this);
        }

        public void Quit()
        {
            if (currentDialogue == null) return;

            TriggerExitActions();
            Cancel();
            
        }

        public bool IsActive()
        {
            return currentDialogue != null;
        }

        public bool IsChoosing()
        {
            return isChoosing;
        }

        public string GetText()
        {
            if(currentNode == null)
            {
                return "";
            }
            return currentNode.GetText();
        }

        public string GetCurrentConversant()
        {
            if(isChoosing)
            {
                return playerName;
            }
            return currentConversant.GetConversantName();
        }

        public IEnumerable<DialogueNode> GetChoices()
        {
            return FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode));
        }
        public void SelectChoice(DialogueNode chosenNode)
        {
            currentNode = chosenNode;
            TriggerEnterActions();
            isChoosing = false;
            Next(); 
        }

        public void Next()
        {
            if (currentDialogue == null) return;
            int numPlayerResponses = FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode)).Count();   
            if (numPlayerResponses > 0)
            {
                isChoosing = true;
                TriggerExitActions();
                onConversationUpdated();
                return;
            }
            DialogueNode[] children = FilterOnCondition(currentDialogue.GetAIChildren(currentNode)).ToArray();
            if (children.Length == 0)
            {
                Quit();
                return;
            }
                
            int i = UnityEngine.Random.Range(0, children.Length);
            TriggerExitActions();
            currentNode = children[i];
            TriggerEnterActions();
            onConversationUpdated();
        }

        public bool HasNext()
        {
            return FilterOnCondition(currentDialogue.GetAllChildren(currentNode)).Count() > 0;
        }
        // Filter the child nodes if certain conditions are not true
        IEnumerable<DialogueNode> FilterOnCondition(IEnumerable<DialogueNode> inputNodes)
        {
            foreach(var node in inputNodes)
            {
                if(node.CheckCondition(GetEvaluators()))
                {
                    yield return node;
                }
            }
        }

        private IEnumerable<IPredicateEvaluator> GetEvaluators()
        {
            return GetComponents<IPredicateEvaluator>();
        }

        // Trigger all the actions on enter that are set on current node
        private void TriggerEnterActions()
        {
            if(currentNode != null )
            {
                foreach(string action in currentNode.GetOnEnterActions())
                {
                    if (action == "") continue;
                    currentConversant.GetComponent<DialogueTrigger>()?.Trigger(action);
                }
            }
        }
        // Trigger all the actions on exit that are set on current node
        private void TriggerExitActions()
        {
            if (currentNode != null)
            {
                foreach (string action in currentNode.GetOnExitActions())
                {
                    if (action == "") continue;
                    foreach (var trigger in currentConversant.GetComponents<DialogueTrigger>())
                    {
                        trigger?.Trigger(action);
                    }
                }
            }
        }

        public void Cancel()
        {
            currentConversant = null;
            currentDialogue = null;
            currentNode = null;
            isChoosing = false;
            isInDialogue = false;
            onConversationUpdated();
        }
    }
}