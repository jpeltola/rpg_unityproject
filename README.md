RPG_UnityProject is an rpg game made with unity. The main reason for this project is been to learn some basics of Unity Engine, C# programming and also some game design elements. 

Engine version is 2019.4.17f1.

Controls:
<ul>
    <li> Mouse click to move </li>
    <li> WASD to rotate Camera </li>
    <li> Mouse wheel to zoom Camera </li>
    <li> Numbers 1-6 for action slots </li>
    <li> E for inventory </li>
    <li> Q for quest log </li>
    <li> O to quick save </li>
    <li> L to quick load </li>
    <li> Del to delete save file </li>
</ul>
This project is made with the GameDev.tv online Unity courses. Many of the features in the game are made following the courses, but many features are added by me.

Some extra features:
<ul>
    <li> Camera controls </li>
    <li> Skills </li>
    <li> Weapon modifiers </li>
    <li> Attribute Effects (Buffs, poison) </li>
</ul>
